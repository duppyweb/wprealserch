jQuery(function($){

    //Generic WP_Ajax call
    function wp_ajax(obj,callback){
        obj.action = "real_search_public_ajax";
        $.ajax({
            "url" : ajaxurl,
            "method": "POST",
            "dataType": "json",
            "success": callback,
            "data" : obj,
            "error" : ajax_error,
            "timeout" : ajax_error
        });
    }
    function ajax_error(){
        //Silently fail
    }

    $(document).ready(function(){
        var search_box = false;
        if ($('input[type="search"]').length){
            search_box = $('input[type="search"]')
        }
        if ($('input[name="s"]').length){
            search_box = $('input[name="s"]')
        }

        if ( search_box ){
            //We got a search box
            search_box.unbind();
            search_box.off();
            var handler = null
            var rs_rate_limit  = 1000;

            function rs_prepare(query,request){
                request.type = "post";
                request.dataType = 'json';
                request.data = {
                    "action" : "real_search_public_ajax",
                    "subaction" : "ajaxSearch",
                    'q' : query
                }
                return request;
            }


            function rs_prepare_prefech(request){
                request.type = "post";
                request.dataType = 'json';
                request.data = {
                    "action" : "real_search_public_ajax",
                    "subaction" : "prefetch"
                }
                return request;
            }

            function rs_transform(resp){
                if (resp.success){
                    return resp.data
                } else {
                    return null;
                }
            }

            function rs_transform_prefetch(resp){
                if (resp.success){
                    if (resp.data){
                        $.cookie('real_search_prefetch_hash', resp.data.hash, {"expires":365});
                        return resp.data.content;
                    } else {
                        return null;
                    }
                } else {
                    return null;
                }
            }

            var rs_hound = new Bloodhound({
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                datumTokenizer: function (item) {
                    return Bloodhound.tokenizers.whitespace(item.title);
                },
                sufficient : 3,
                identify: function(obj) { return obj.id; },
                prefetch: {
                    cache: false,
                    url: ajaxurl+'?rnd='+ $.cookie('real_search_prefetch_hash'),
                    prepare : rs_prepare_prefech,
                    transform : rs_transform_prefetch
                },
                remote : {
                    url : ajaxurl,
                    prepare : rs_prepare,
                    rateLimitWait : rs_rate_limit,
                    rateLimitBy : 'debounce',
                    transform : rs_transform
                }
            });
            //Hook the typehead
            var timeout;
            search_box.typeahead({
                    minLength: 1,
                    highlight: false
                },{
                display: function(data){
                    return '';
                    var tmp = document.createElement("DIV");
                    tmp.innerHTML = data.title;
                    return tmp.textContent || tmp.innerText || "";
                },
                source: rs_hound,
                templates : {
                    suggestion : function(data){
                        return '<div class="rs_sbox"><a href="'+data.link+'">'+data.title+'</a> </div>';
                    },
                    pending : function(data){
                        return '<h1 class="blink">Searching...</h1>';
                    },
                    notFound : function(data){
                        return '<h1>Press enter to full search</h1>';
                    }
                }
            });

        }

    });
});