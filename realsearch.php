<?php
/**
 * Plugin Name: RealSearch
 * Description: RealSearch plugin for WordPress. Search like it's meant to be.
 * Version:     1.1.0
 * Text Domain: real-search
 * Domain Path: /languages
 * Author:      RealSearch
 * Author URI:  http://wprealsearch.com/
 * License:  GPLv2
 */

define("REALSEARCH_DEBUG", false);
define("REALSEARCH_LOG_LEVEL", 10);
define("REALSEARCH_DEBUG_FB", false);
define("REALSEARCH_DEMO_STATS", false);
defined( 'ABSPATH' ) or die( 'Silence is golden!' );

Class RealSearch {

    protected $version;
    protected $log_file;
    public $slug = 'realsarch';
    public $sm = null;
    public $dash;
    public $utils;
    public $rsql;
    public $stats;
    public $settings = array();
    public $option_key = 'real_search_settings';
    public $api_url = 'http://api.wprealsearch.com/api/';
    public $global = array();
    public $id_counter = array();

    protected $active_theme = 'default';
    protected $search_id = '0';
    protected $lucene_index;
    protected $lucene_index_base;
    protected $vars = array('swq');
    protected $magic_int = 8252387255;
    protected $path = null;



    function __construct()
    {
        $this->version = '1.1.0';
        $this->path = dirname(__FILE__);
        libxml_use_internal_errors(true);


        //Load Settings
        $this->settings = $this->get_settings();

        //Set the active theme, for theme related fixes and for LiveSearch
        $this->active_theme = get_option('template');

        //Load theme related settings
        $theme_file = trailingslashit($this->path).'theme_php/'.$this->active_theme.'.php';
        /*
         *
         */
        if ( file_exists( $theme_file ) && strpos( realpath( $theme_file ), realpath(trailingslashit($this->path).'theme_php/') ) == 0  )
        {
            include_once $theme_file;
        }

        //Register WP Specific Hooks
        $this->register_hooks();

    }

    /**
     * Runs at WP Init
     */
    public  function init()
    {
        //Load Translations
        $domain = 'real_search';
        $locale = apply_filters( 'plugin_locale', get_locale(), $domain );
        if ( $loaded = load_textdomain( $domain, trailingslashit( WP_LANG_DIR ) . $domain . '/' . $domain . '-' . $locale . '.mo' ) )
        {
            //It is already loaded, nothing to do
        }
        else
        {
            load_plugin_textdomain( $domain, FALSE, basename( dirname( __FILE__ ) ) . '/languages/' );
        }

        //Opens the index
        if ( $this->open_index() )
        {
            //Init Search Module, will ba accessible via $this->sm
            $this->init_search_module();
        }

        if ( $this->settings['plugin_enabled'] === true && $this->sm !== null )
        {
            //Catch Get Posts
            add_action('pre_get_posts', array($this, 'pre_get_posts'));

            //Catch search Queryies and FALSE them
            add_filter( 'posts_search', array($this,'posts_search'),10,2);

            //Catch excerpt, and if it is a search highlight it.
            //add_filter( 'get_the_excerpt', array($this,'highlight_excerpt'),PHP_INT_MAX - 1000 ,2);
            add_filter( 'the_excerpt', array($this,'highlight_excerpt'),PHP_INT_MAX - 1000 ,2);

            //Catch title, and if it is a search highlight it.
            add_filter( 'the_title', array($this,'highlight_title'),10,2);

            //Catch perma_link, and add tracking URL Fragment
            add_filter( 'post_link', array($this,'post_link'), PHP_INT_MAX - 1000, 3 );

            //Do the Real Search if $->is_search
            add_filter( 'the_posts', array($this,'the_posts') , PHP_INT_MAX - 1000, 2);

            //Catch content as arrives Posts and Pages <= Index
            add_action( 'save_post', array( $this, 'index_post' ) , PHP_INT_MAX - 1000, 1);

            //Catch Attachment as it arrives <= Index
            add_action( 'add_attachment', array($this, 'attachment'), PHP_INT_MAX - 1000, 1);
            add_action( 'edit_attachment', array($this, 'attachment'), PHP_INT_MAX - 1000, 1);
            add_action( 'delete_attachment',array($this,'attachment_delete'), PHP_INT_MAX - 1000, 1);

            //Catch Comments as they arrive <= Index
            add_action( 'edit_comment', array($this, 'index_comment'), PHP_INT_MAX - 1000, 1);
            add_action( 'wp_insert_comment', array($this, 'index_comment'), PHP_INT_MAX - 1000, 1);
            add_action( 'delete_comment', array($this, 'delete_comment' ), PHP_INT_MAX - 1000, 1);
            add_action( 'trash_comment', array($this, 'delete_comment' ), PHP_INT_MAX - 1000, 1 );

        }


        //Init Statistics engine
        $this->init_stats();

        //Init Dashboard will ba accessible via $this->dash
        if ( is_admin() )
        {
            $this->init_dash();
        }
    }

    public  function pre_get_posts($query)
    {
        return $query;
        //var_dump($query);
    }


    /**
     * Register WP Specific Hooks
     *
     */
    private function register_hooks()
    {
        //Plugin Update Checker
        $this->puc();

        //Activations
        register_activation_hook( plugin_basename(__FILE__), array( $this, 'activate' ) );
        register_deactivation_hook( plugin_basename(__FILE__), array( $this, 'deactivate' ) );

        //The Init Hook
        add_action( 'init', array($this, 'init') );

        add_shortcode('rsdebug_echo', function(){echo "<h1>RSDEBUG</h1>";});
        add_shortcode('rsdebug_500', function(){ this_will_cause_500(); });

        //Plugin notices
        add_action( 'admin_notices', array($this,'notices') );

        //Admin menu
        add_action('admin_menu', array($this,'admin_menu'), 10);

        //Admin menu JS CSS etc
        add_action('admin_enqueue_scripts', array($this,'admin_menu_head'),10);


        //Public JS CSS
        add_action('wp_enqueue_scripts', array($this,'public_head'),10);


        //Admin menu AJAX
        if ( is_admin() )
        {
            add_action('add_meta_boxes', array($this, 'add_meta_box') );
            add_action("wp_ajax_real_search_ajax", array($this, 'admin_ajax'));
        }

        //Public AJAX endpoint
        add_action("wp_ajax_real_search_public_ajax", array($this, 'public_ajax'));
        add_action("wp_ajax_nopriv_real_search_public_ajax", array($this, 'public_ajax'));

        //WP Head, for ajaxurl js variable
        add_action('wp_head', array($this, 'wp_head'));

    }


    /**
     * Does the real search, using Lucene Indexes.
     *
     * @param $posts
     * @param $query
     * @return array|null
     */

    public function the_posts($posts,$query)
    {
        if ($query->is_search && !is_admin())
        {
            //Search
            rs_log(1,$query->query_vars['s'],'Query');
            rs_log(3,$query->query_vars,'Got Query');

            //Get the current page number form WP
            $paged = (isset($query->query['paged'])) ? (int)$query->query['paged'] : 0;

            //Get Post Per Page setting
            $ppp = get_option('posts_per_page');

            //Start for array_slice = page number * post per page
            $start = $paged * $ppp;


            $result = $this->sm->find($query->query_vars, $query, $start, $ppp);
            if ( empty($result) )
            {
                //Do a standard SQL Search v1.1.0
                //Do a fuzzy search if there less then 5 words
                $query = (string)$query->query_vars['s']; //TODO read value from WP 's'
                $query = explode(' ',$query);
                $query = array_map("trim",$query);

                if (count($query)<5)
                {
                    //We are good
                    $fuzzy = '';
                    foreach ($query as $part)
                    {
                        $fuzzy .= $part.'~0.7';
                    }
                    $fake_query = array('s'=>$fuzzy);
                    $result = $this->sm->find($fake_query, $query, $start, $ppp);
                }

            }
            if ($start == 0 )
            {
                $this->search_id = $this->stats->log_search($query->query_vars, count($result));
            }

            //Pagination count
            $count = count($result);

            //Calculate total pages
            $query->max_num_pages = floor($count / $ppp);
            $query->found_posts = count($result);

            //Slice the result and return it
            $result = ( is_array($result) ) ?  array_slice($result,$start,$ppp) : array();


            if (empty($result))
            {
                return null;
            }
            //Return the results
            return $result;
        }
        //Return original posts if is not a search
        return $posts;

    }



    /**
     * Embeds excerpt or title for ajax to use
     * @params $id Post_id
     * @params $text Original excerpt
     */
    public function embed_for_ajax($id, $text, $comment_id = 0)
    {
        $id = (int)$id;
        $query_key = ( isset( $this->sm->query_key ) ) ? $this->sm->query_key : '0000';
        return '<span class="rs-hl-ajax" data-hl-comment-id="'.esc_attr($comment_id).'" data-hl-id="'. esc_attr($id) .'" data-hl-qk="'.esc_attr($query_key).'"  >'.$text.'</span>';
    }

    /**
     * Adds tracking tag to searach result URLs
     */
    public function post_link($url, $post = null, $leaveme = null)
    {
        if (is_search() && !is_admin() && is_main_query() && in_array($post->ID, $this->sm->set ))
        {
               $url = $url.'#rs-'.$this->search_id;
        }
        if ($post == null && defined('IS_AJAX') && IS_AJAX == true)
        {
            $url = $url.'#rs-'.$this->search_id;
        }
        return $url;
    }

    /**
     * Highlight the excerpt on search pages, OR shows comment
     * @param $excerpt
     * @return string
     */
    public function highlight_excerpt($excerpt)
    {

        //Comments are a special case
        if  ($this->settings['comments'] && is_search() && !is_admin() && is_main_query())
        {
            $id = get_the_ID();

            if (isset($GLOBALS['hits'][$id]))
            {
                //If there where no "queries" to this ID, we assume it is the first
                if (!isset($this->id_counter[$id]) )
                {
                    $this->id_counter[$id] = 0;
                }
                else
                {
                    $this->id_counter[$id] = $this->id_counter[$id] + 1;
                }
                $index = $this->id_counter[$id];
                if (isset($GLOBALS['hits'][$id][$index]))
                {
                    $item  = $GLOBALS['hits'][$id][$index];
                    if ($item['is_comment'])
                    {
                        //Check for highlight
                        if ( $this->settings['highlight'] && isset($GLOBALS['comment_hl'][$id][$item['comment_id']]['content']))
                        {
                            //Return the highlight
                            return $GLOBALS['comment_hl'][$id][$item['comment_id']]['content'];
                        }
                        else
                        {
                            $comment = get_comment($item['comment_id']);
                            $comment_content =  apply_filters('comment_text', $comment->comment_content);
                            //Ajax HL is enabled and we know about the ID
                            if ($this->settings['highlight_ajax'] === true && isset($GLOBALS['comment_hl'][$id]) )
                            {
                                return $this->embed_for_ajax( $id, $comment_content, $item['comment_id']);//"FOR_AJAX";
                            }
                            //Return the comment content.
                            return $comment_content;
                        }
                    }
                    else
                    {
                        //Could be a post or something else
                        $this->id_counter[$id] = $this->id_counter[$id] - 1;
                    }

                }

            }

        }
        if  ( $this->settings['highlight'] && is_search() && !is_admin() && is_main_query() )
        {
            //If we have it return it highlighted
            $id = get_the_ID();
            if (get_post_type() === 'attachment')
            {
                return wp_get_attachment_image($id, array(640,640));
            }

            //We have a highlighted text
            if ( isset( $this->sm->hl[$id]['content']) && $this->sm->hl[$id]['content'] !== '' )
            {
                return $this->sm->hl[$id]['content'];//"HIGHLIGHTED";
            }

            //Ajax HL is enabled and we know about the ID
            if ($this->settings['highlight_ajax'] === true && isset($this->sm->hl[$id]) )
            {
                return $this->embed_for_ajax($id,$excerpt);//"FOR_AJAX";
            }

        }
        return $excerpt;//"DEFAULT";
    }

    /**
     * Highlight the excerpt on search pages
     * @param $title
     * @return string
     */
    public function highlight_title($title)
    {

        $id = get_the_ID();

        if  ($this->settings['comments'] && $this->settings['highlight'] && is_search() && !is_admin() && is_main_query())
        {
            if (isset($GLOBALS['comment_hl'][$id][md5($title)]['title']))
            {
                return $GLOBALS['comment_hl'][$id][md5($title)]['title'];
            }
        }
        if  ( $this->settings['highlight'] && is_search() && !is_admin() && is_main_query() || (defined('DOING_AJAX') && DOING_AJAX) )
        {
            if ($id === false )
            {
                //It is called outside of the loop
                $id = isset($this->global['ajax_search_id']) ? $this->global['ajax_search_id'] : false;
                //Maybe add $wp_query->post->ID
            }
            if ( isset( $this->sm->hl[$id]['title'] ) && (md5($title) === $this->sm->hl[$id]['md5_hash']) )
            {
                return $this->sm->hl[$id]['title'];
            }
            // Return the default.
            return $title;
        }
        return $title;
    }

    /**
     * Replaces the WHERE part of Search queries
     * to a FALSE value, in order to avoid double searching.
     *
     * @param $param
     * @param $query
     * @return string SQL after WHERE part
     */
    public function posts_search( $param, $query)
    {
        if (!$query->is_search )
        {
            return $param;
        }
        if ( $this->settings['kill_sql_search'] == true && $query->is_search && $query->is_main_query() && !is_admin() )
        {
            rs_log(8,$param);
            return "AND 1=".$this->magic_int."  -- ";
        }
        return $param;
    }

    /**
     * Pre processes documents before passing to Serach Module for indexing
     * All WP Releated Extractions, Parsing, Taxonomy etc, should be done here
     * @param $post_id
     * @return mixed
     */
    public function index_post( $post_id )
    {
        //Get the post as array
        $post = get_post( $post_id );

        //In case an attachment got here
        if ($post->post_type == 'attachment')
        {
            rs_log(3,$post,'Sending to attachment');
            return $this->attachment( $post_id );
        }

        rs_log(3,$post,'Got Post');

        //Get the "Primary" Key
        $pkid = $this->sm->generate_id( $post_id );

        //Delete the post if needed
        if ( $this->should_delete( $post ) )
        {
            //Then delete form the index
            $this->sm->delete_doc_by_id( $pkid );
            rs_log(3,$post,'Deleting post PKID:'.$pkid);

        }
        if ( $this->should_index( $post ) )
        {
            rs_log(3, "Should index ".$post->ID);
            //Delete for duplicates
            $this->sm->delete_doc_by_id($pkid);
            rs_log(3,'Deleting post for de dupe PKID:'.$pkid);

            //Add to index form the index
            $doc = array();
            $doc['pkid_Lucene'] = $pkid;
            $post = $this->utils->prepare_post($post);
            //Parse all of it
            foreach ( array_keys($post) as $var)
            {
                //Grab any output from shorctodes
                ob_start();
                //Handle special cases
                if ($var == 'post_title')
                {
                    $value = apply_filters( 'the_title', $post['post_title']);
                }
                if ($var == 'post_content')
                {
                    $value = apply_filters( 'the_content', $post['post_content'] );
                    $value = strip_shortcodes( $value );
                    $value = html_entity_decode( $value );
                    $value = strip_tags( $value );
                }
                //We do not need it for now.
                $output = ob_get_clean();

                //Turn the rest to string
                if ( !in_array( gettype($post[$var]), array('boolean','integer','double','string') ) )
                {
                    $tmp = json_encode( $post[$var] );
                    $tmp = json_decode( $tmp, true );
                    try
                    {
                        $value = '';
                        $it = new RecursiveIteratorIterator(new RecursiveArrayIterator($tmp));
                        foreach ( $it as $v )
                        {
                            $value .= ' <> '.$v;
                        }
                    }
                    Catch (Exception $e)
                    {
                        //Some error
                    }

                }
                else
                {
                    $value = (string)$post[$var];
                }
                //Set the value
                $doc[$var] = ( isset($value) )  ? $value : null;
            }

            //Unify content
            $doc['content_Lucene'] = $doc['post_content'];
            unset($doc['post_content']);

            rs_log(3,$doc,'Indexing Post');

            try
            {
                $this->sm->index_doc($doc);
            }
            catch (Exception $e)
            {
                //rs_log(1,'[ERROR_INDEXING] '.$e->getMessage());
            }
        }
    }

    /**
     * Index attachment, if it is readable format (doc, docx, pdf, xls)
     *
     * @param $att_id
     * @return bool
     */
    public function attachment( $att_id )
    {
        //We got some attachment action going on
        $attachment = get_post( $att_id );
        rs_log(5,$attachment,"Got Attachment ID:".$att_id);

        if ( !$this->should_index( $attachment ) )
        {
            return false;
        }
        //Delete the attachment before re index;
        $this->attachment_delete($att_id);

        //Check if the file still exists.
        $path = get_attached_file( $att_id );
        $pkid = $this->sm->generate_id( $att_id );

        if (@file_exists( $path ))
        {

            $ext = strtolower( pathinfo( $path, PATHINFO_EXTENSION) );

            //Add to index form the index
            $doc = array();

            $doc['pkid_Lucene'] = $pkid;
            $attachment = $this->utils->prepare_post( $attachment );

            //Parse all of it
            foreach ($attachment as $var=>$value)
            {
                //Handle special cases // Leave it here just in case
                if ($var == 'post_title')
                {
                    $value = apply_filters( 'the_title', $attachment['post_title'] );
                    rs_log(4,"Titlte: ".$value);
                }

                //Turn the rest to string
                if ( !in_array( gettype($attachment[$var]), array('boolean','integer','double','string') ) )
                {
                    $tmp = json_encode( $attachment[$var] );
                    $tmp = json_decode( $tmp, true );
                    try
                    {
                        $value = '';
                        $it = new RecursiveIteratorIterator(new RecursiveArrayIterator($tmp));
                        foreach ( $it as $v )
                        {
                            $value .= ' <> '.$v;
                        }
                    }
                    Catch (Exception $e)
                    {
                        //Some error
                    }

                }
                else
                {
                    $value = (string)$attachment[$var];
                }
                //Set the value
                $doc[$var] = $value;
            }
            //Documents will be handled by Lucene
            if ( preg_match('~(doc|docx|xlsx|pptx|pdf)$~isu', $ext) )
            {
                //Mark for search module that is an attachment
                $doc['content_Lucene'] = array('path'=>$path, 'ext'=>$ext);
            }
            if ( preg_match('~^image/~', $attachment['post_mime_type'] ))
            {
                //Images will be handled by CloudVison API
                rs_log(3,"We should index it as an Image");
                require_once $this->path. '/vision.php';
                try
                {
                    $vision = new RealSearchVision($this->settings, $path );

                    $text = $vision->vision();
                    rs_log(4,"Google vision text: ".$text);
                    update_post_meta( $att_id, 'real_search_text_google', $text);

                }
                catch (Exception $e)
                {
                    rs_log(1,"Vision error: ".$e->getMessage());
                }

                if ( is_string( $text ) )
                {
                    $doc['content_Lucene']  = $text;
                }
                else
                {
                    return false;
                }

            }




            rs_log(4,"Try to index Attachment ".$path);
            rs_log(6,$doc,"Indexing Attachment");
            try
            {
                $this->sm->index_doc($doc);
                //TODO save text as post meta
            }
            catch (Exception $e)
            {
                rs_log(1,'[ERROR_INDEXING] '.$e->getMessage());
            }

        }
        else
        {
            rs_log(1,"File does not exists: ".$this->path);
        }

    }

    /**
     * Deletes an attachment from the Index.
     * @param $att_id
     * @return bool
     */
    public function attachment_delete($att_id)
    {
        $pkid = $this->sm->generate_id($att_id);
        rs_log(1,"Deleted Attachment from Index : ".$pkid);
        $this->sm->delete_doc_by_id($pkid);
        return true;
    }

    /**
     * Indexes a comment
     * @param $comment_id
     */
    public function index_comment($comment_id)
    {
        $comment_id = (int)$comment_id;
        $comment = get_comment($comment_id);

        //Get the "Primary" Key
        $pkid = $this->sm->generate_id( $comment_id, 'comment' );

        if ($this->should_delete_comment($comment))
        {
            rs_log(3,'Deleting Comment, type change PKID:'.$pkid);
        }

        if ($this->should_index_comment($comment))
        {
            rs_log(3, "Should index comment:".$comment_id);
            //Delete for duplicates
            $this->sm->delete_doc_by_id($pkid);
            rs_log(3,'Deleting comment for de dupe PKID:'.$pkid);

            //Add to index form the index
            $doc = array();
            $doc['pkid_Lucene'] = $pkid;
            $comment = $this->utils->prepare_comment($comment);
            //Parse all of it
            foreach ( array_keys($comment) as $var)
            {
                if ($var === 'comment_content')
                {
                    $value = apply_filters('comment_text', $comment['comment_content']);
                }
                else
                {
                    $value = $comment[$var];
                }

                $doc[$var] = $value;
            }
            $doc['content_Lucene'] = $doc['comment_content'];
            unset($doc['comment_content']);

            rs_log(3,$doc,'Indexing Comment');
            try
            {
                $this->sm->index_doc($doc);
            }
            catch (Exception $e)
            {
                //rs_log(1,'[ERROR_INDEXING] '.$e->getMessage());
            }

        }

    }

    public function delete_comment($comment_id)
    {
        $comment_id = (int)$comment_id;
        $comment = get_comment($comment_id);

        //Get the "Primary" Key
        $pkid = $this->sm->generate_id( $comment_id, 'comment' );

        $this->sm->delete_doc_by_id($pkid);
        rs_log(3,'Deleting Comment PKID:'.$pkid);

    }
    /**
     * Should we delete the post and don't index it usually?
     *
     * @param $post WP_Post
     * @return boolean
     */
    public function should_delete($post)
    {
        $post = (array)$post;
        //If there is no status, don't delete from the index
        if ( !isset($post['post_status']) )
        {
            return false;
        }
        $post['post_password'] = (isset($post['post_password'])) ? $post['post_password'] : '';
        if (
            $post['post_status'] === 'private' || $post['post_status'] === 'draft' || $post['post_status'] === 'pending' ||
            $post['post_password'] !== '' || $post['post_status'] == 'trash'
        )
        {
            return true;
        }

        return false;
    }

    /**
     * Should we delete the comment and don't index it usually?
     *
     * @param $comment WP_Comment
     * @return boolean
     */
    public function should_delete_comment($comment)
    {
        $comment = (array)$comment;
        if (isset($comment['comment_approved']) && $comment['comment_approved'] == 1)
        {
            return false;
        }
        return true;

    }


    /**
     * Should it index the post
     * @param $post WP_Post
     *
     * @return boolean
     */
    public function should_index($post)
    {
        if (is_numeric($post))
        {
            $post = get_post($post);
        }
        $post->post_password = ( isset($post->post_password) ) ? $post->post_password : '';
        //If its Publish Status and there is no password we index it.

        $this->init_query();
        $this->rsql->post = $post;
        $this->rsql->load_mappings();
        if ( ($post->post_password == '') &&  $this->rsql->set_query( $this->settings['selector_text'] )->match() )
        {
            return true;
        }
        //By default return false
        return false;
    }

    public function should_index_comment($comment)
    {
        if ($this->settings['comments'] == false)
        {
            return false;
        }
        $comment = (array)$comment;
        if (
            isset($comment['comment_approved']) && $comment['comment_approved'] == 1
            && isset($comment['comment_type']) && $comment['comment_type'] == '' //No pingback or linkback tracking
        )
        {
            return true;
        }
        return false;
    }
    /**
     * Adds JS / CSS to wordpress Frontend / public
     */
    public function public_head()
    {
        wp_enqueue_script('jquery');
        wp_enqueue_script('real_search_public_js',  plugins_url( 'tpl/js/real_search_public.js' , __FILE__ ), array('jquery') );
        wp_enqueue_script('jquery.cookie.js',  plugins_url( 'tpl/js/jquery.cookie.js' , __FILE__ ), array('jquery') );
        if ( isset($this->settings['ajax_search']) && $this->settings['ajax_search'] == true || true )
        {
            //Support for life search
            $theme_name = $this->active_theme;
            $base = trailingslashit($this->path). 'tpl/ajax_search_themes/';

            $file_name = file_exists( $base.$theme_name.'.css' ) ? $theme_name : 'default';
            wp_enqueue_style( 'real_search_ajax_search_css', plugins_url( 'tpl/ajax_search_themes/'.$file_name.'.css' , __FILE__ ) );

            $file_name = file_exists( $base.$theme_name.'.js' ) ? $theme_name  : 'default';
            wp_enqueue_script('real_search_ajax_search_js',  plugins_url( 'tpl/ajax_search_themes/'.$file_name.'.js' , __FILE__ ), array('jquery') );

            wp_enqueue_script('real_search_bh_js',  plugins_url( 'tpl/js/bloodhound.js' , __FILE__ ), array('jquery') );
            wp_enqueue_script('real_search_th_js',  plugins_url( 'tpl/js/typeahead.jquery.js' , __FILE__ ), array('jquery') );
        }

    }

    /**
     * Public ajax endpoints
     */
    public function public_ajax()
    {
        require_once $this->path.'/ajax.php';

        $subaction = (isset($_POST['subaction'])) ? (string)$_POST['subaction'] : 'ping';
        $ajax = new RealSearchAjax($this);

        if ( in_array($subaction, $ajax->endpoints_public) )
        {
            call_user_func(array($ajax, $subaction));
        }
    }

    /**
     * Main ajax endpoint, verifies access and inits Admin Ajax
     */
    public function admin_ajax()
    {
        if ( !wp_verify_nonce($_POST['token'], 'real_search_admin') || !current_user_can('manage_options') ){
            wp_send_json_error( "Invalid token" );
        }
        $subaction = (isset($_POST['subaction'])) ? (string)$_POST['subaction'] : 'ping';

        require_once $this->path.'/ajax.php';

        $ajax = new RealSearchAjax($this);

        if ( in_array($subaction, $ajax->endpoints) )
        {
            call_user_func(array($ajax, $subaction));
        }

    }
    public function add_meta_box()
    {
        add_meta_box( 'real_search_status', __( 'RealSearch Status', 'real_search' ), array($this, 'meta_box'), null );
    }

    /**
     * Generates a simple metabox with the text
     * Index status: Indexex/Not Indexed, Should index: YES/NO
     */
    public function meta_box()
    {
        $id = get_the_ID();
        $index_status = $this->sm->indexed($id);
        if ( $this->sm == null ||   $index_status  == null )
        {
            $indexed =  esc_html__('Error', 'real_search');
        }
        else
        {
            $indexed = ($index_status) ? esc_html__('Indexed', 'real_search') : esc_html__('Not indexed', 'real_search');
        }
        $should_index = $this->should_index($id) ? esc_html__('YES', 'real_search') : esc_html__('NO', 'real_search');


        $str  =   esc_html__('Index status: ', 'real_search');
        $str .= "<b>".$indexed."</b>, ";
        $str .=   esc_html__('Should index: ', 'real_search');
        $str .= "<b>".$should_index."</b>";
        $text = get_post_meta($id, 'real_search_text_google', true );
        if ( $text )
        {
            $str .= "<br/><b>". esc_html__('Google Cloud Vision Text: ', 'real_search');
            $str .= "</b><br/><i>". esc_html($text) .'<i>';
        }

        echo $str ;

    }

    /**
     * Adds Admin menu into "Settings"
     */
    public function admin_menu()
    {
        add_menu_page('ReaSearch', 'RealSearch', 'manage_options', 'RealSearch' , '', 'dashicons-search'  );
        add_submenu_page('RealSearch', 'RealSearch Settings', 'Settings', 'manage_options', 'RealSearch' , array($this->dash, 'settings' ) );
        add_submenu_page('RealSearch', 'RealSearch Settings', 'Stats', 'manage_options', 'RealSearchStats' , array($this->dash, 'stats' ) );
    }

    /**
     * Get Real Search WP Settings
     *
     * @return array $settings
     */
    public function get_settings()
    {
        $default =  array(  'index_post_types'=>array('post','attachment','page'),
                            'highlight'=>true,
                            'locale'=>'enEN',
                            'hl_html'=>'<mark>[WORD]</mark>',
                            'google_vision_key' => '',
                            'highlight_ajax'=>true,
                            'ajax_search'=>true,
                            'plugin_enabled'=>true,
                            'kill_sql_search'=>true,
                            'purchase_key'=>'',
                            'comments'=>false,
                            'selector_text' => '( post.type IN ( "post", "page", "product" ) AND post.status = "publish"  ) OR
( post.type = "attachment" AND post.status IN ( "inherit", "publish" ))
'
                         );
        return get_option($this->option_key,$default);
    }




    /**
     * Adds JS / CSS to header part in the Admin menu
     */
    public function admin_menu_head($hook)
    {
        if ( !preg_match('~RealSearch(Stats)?$~', $hook) )
        {
            return true;
        }
        wp_enqueue_script('jquery');
        wp_enqueue_script('jquery.cookie.js',  plugins_url( 'tpl/js/jquery.cookie.js' , __FILE__ ), array('jquery') );

        wp_enqueue_script('real_search_admin_js',  plugins_url( 'tpl/js/real_search_admin.js' , __FILE__ ), array('jquery') );
        wp_enqueue_style( 'real_search_admin_css', plugins_url( 'tpl/css/style.css' , __FILE__ ) );

        //Caret and At.js for RS-QL
        wp_enqueue_style( 'jquery.atwho.css', plugins_url( 'tpl/css/jquery.atwho.css' , __FILE__ ) );
        wp_enqueue_script('jquery.atwho.js',  plugins_url( 'tpl/js/jquery.atwho.js' , __FILE__ ), array('jquery') );
        wp_enqueue_script('jquery.carret.js',  plugins_url( 'tpl/js/jquery.carret.js' , __FILE__ ), array('jquery') );

        //DataTable
        wp_enqueue_script('jquery.datatable.js',  plugins_url( 'tpl/js/jquery.dataTables.js' , __FILE__ ) , array('jquery') );
        wp_enqueue_style( 'jquery.datatable.css',  plugins_url( 'tpl/css/jquery.dataTables.min.css' , __FILE__ ) );

        //Stats
        wp_enqueue_style( 'MaterialIcons', "https://fonts.googleapis.com/icon?family=Material+Icons" );
        wp_enqueue_style( 'MaterialIcons', "https://fonts.googleapis.com/icon?family=Material+Icons" );
        wp_enqueue_style( 'vizuly.css', plugins_url( 'tpl/css/vizuly.css' , __FILE__ ) );

        //Stats Cloud
        wp_enqueue_script('d3.js',  plugins_url( 'tpl/js/d3.js' , __FILE__ ) );
        wp_enqueue_script('d3.layout.cloud.js',  plugins_url( 'tpl/js/d3.layout.cloud.js' , __FILE__ ), array('d3.js') );
        wp_enqueue_script('d3.wordcloud.js',  plugins_url( 'tpl/js/d3.wordcloud.js' , __FILE__ ), array('d3.js') );

        //Stats Graph
        wp_enqueue_script('vizuly_core.min.js',  plugins_url( 'tpl/js/vizuly_core.min.js' , __FILE__ ), array('d3.js') );
        wp_enqueue_script('vizuly_linearea.min.js',  plugins_url( 'tpl/js/vizuly_linearea.min.js' , __FILE__ ), array('vizuly_core.min.js') );
        wp_enqueue_script('real_search_graph.js',  plugins_url( 'tpl/js/real_search_graph.js' , __FILE__ ), array('vizuly_core.min.js') );


    }

    /**
     * Outputs the ajax url just to make sure that we have it
     */
    public function wp_head()
    {
        echo "<script> var ajaxurl = '". admin_url('admin-ajax.php'). "';</script>";
    }

    /**
     * Initializes the Dashboard Module
     */
    public function init_dash()
    {
        require_once $this->path.'/dashboard.php';
        $this->dash = new RealSerachDashboard($this->settings, $this->version);
    }

    /**
     * Initializes the Search Module
     * The Search Module is responsable for indexing, re indexing and searching.
     * It provides low level Indexing and searching, nothing WordPress specific.
     */
    public function init_search_module(){
        try
        {
            require_once $this->path.'/searchmodule.php';
            require_once $this->path.'/utils.php';
            $this->utils = new RealSearchUtils();
            $this->sm = new RealSearchModule($this->lucene_index, $this->settings);
        }
        catch (Exception $e)
        {
            $this->show_error( sprintf( esc_html__( "Can't initialize Search Module. Message: %s"), $e->getMessage())  , E_USER_ERROR);
            return false;
        }

        //Search module Initiailzed OK, we can remove the error notices.
        set_transient( 'real_search_notices_admin_error', null );
    }

    /**
     * Init stats engine
     * Logging search phases, building statistics
     */
    public function init_stats()
    {
        try
        {
            require_once $this->path.'/stats.php';
            $this->stats = new RealSeachStats();
        }
        catch (Exception $e)
        {
            rs_log(1, "Cant init Stats Engine");
        }
    }

    /**
     * Init RealSearchQuery parser and resolver
     * Used internally for validating RS-QL and matching Posts to it
     */
    public function init_query()
    {
        try
        {
            require_once $this->path.'/query.php';
            $this->rsql = new RealSearchQuery();
        }
        catch (Exception $e)
        {
            rs_log(1, "Cant init RS-Query");
        }
    }


    /**
     * Called when the plugin is activated / installed
     */
    public function activate()
    {
        $this->open_index();
        $this->create_schema();
        rs_log(1,'Plugin activated');
    }
    /**
     * Called when the plugin is deactivated / uninstalled
     */
    public function deactivate()
    {
        rs_log(1,'Plugin Deactivated');
    }

    /**
     * Opens an Index
     * If it is non existent it tries to create one, if it fails triggers an error
     */
    public function open_index()
    {
        require_once $this->path.'/libs/SearchModule/vendor/autoload.php';


        $this->lucene_index_base = trailingslashit(WP_CONTENT_DIR).'realsearch';
        $this->lucene_index = trailingslashit($this->lucene_index_base).'by-id-'.get_current_blog_id();

        if ( !file_exists( $this->lucene_index_base ) )
        {
            try
            {
                $dir = @mkdir($this->lucene_index_base);
            }
            catch (Exception $e)
            {
                $msg = sprintf( esc_html__("Can't create RealSearch Index folder (%s). Please fix the file permission errors", 'real_search'), $this->lucene_index_base );
                rs_log(1,$msg);
                $this->show_error($msg, E_USER_ERROR);
            }
        }

        if ( !file_exists($this->lucene_index) )
        {
            try
            {
                $dir = @mkdir($this->lucene_index);
            }
            catch (Exception $e)
            {
                $msg = sprintf( esc_html__("Can't create RealSearch Index by site folder (%s). Please fix the file permission errors", 'real_search'), $this->lucene_index );
                rs_log(1,$msg);
                $this->show_error($msg, E_USER_ERROR);
            }
        }

        if (!is_writable($this->lucene_index))
        {
            $msg = sprintf( esc_html__("RealSearch Index folder is not writable! (%s). Please change its permission.", 'real_search'), $this->lucene_index );
            rs_log(1,$msg);
            $this->show_error($msg, E_USER_ERROR);
        }

        if (!is_readable($this->lucene_index))
        {
            $msg = sprintf( esc_html__("RealSearch Index folder is not readable! (%s). Please change its permission.", 'real_search'), $this->lucene_index );
            rs_log(1,$msg);
            $this->show_error($msg, E_USER_ERROR);
        }
        return true;

    }
    /**
     * Creates database tables, OCR cache and stats
     *
     */
    public function create_schema()
    {
        global $wpdb;

        require_once( ABSPATH . 'wp-admin/includes/upgrade.php' );
        $charset_collate = $wpdb->get_charset_collate();

        $table_name = $wpdb->prefix . "rs_stats";
        $sql = "CREATE TABLE $table_name (
            `id` int(11) NOT NULL AUTO_INCREMENT,
            `site_id` int(11) NOT NULL,
            `query` text CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
            `hash` varchar(32) NOT NULL,
            `result` int(11) NOT NULL,
            `hits` int(11) NOT NULL,
            `stamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
            PRIMARY KEY (id),
            UNIQUE KEY hash (hash(32)),
            INDEX site_id (site_id)
            )  $charset_collate";
        try
        {
            dbDelta( $sql );
        }
        Catch (Exception $e)
        {
            rs_log(1, $e->getMessage(), "DB Delta activate");
        }


        $table_name = $wpdb->prefix . "rs_vision_cache";
        $sql = "CREATE TABLE $table_name (
          id INT(11) NOT NULL AUTO_INCREMENT,
          md5 VARCHAR(32) NOT NULL,
          image_text TEXT CHARACTER SET utf8 COLLATE utf8_bin NOT NULL ,
          raw_response TEXT NOT NULL,
          stamp TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
          PRIMARY KEY (id),
          INDEX md5 (md5(32))
          )  $charset_collate";
        try
        {
            dbDelta( $sql );
        }
        Catch (Exception $e)
        {
            rs_log(1, $e->getMessage(), "DB Delta activate");
        }

    }

    /**
     *  Optimizes Index.
     *
     * It should be called from the Admin interface
     */
    public function optimize()
    {
        $this->sm->optimize();
    }

    /**
     * Displays error messages.
     * @param $msg string
     * @param $flag
     */
    public static function show_error($msg,$flag)
    {
        if (debug_backtrace()[1]['function'] == 'activate')
        {
            trigger_error($msg, $flag);
        }
        else
        {
            $notices = get_transient('real_search_notices_admin_error');
            $transient = array('flag'=>$flag, 'msg'=>$msg);
            $notices[ md5( json_encode($transient) ) ] = $transient;
            set_transient( 'real_search_notices_admin_error', $notices, 60*60*12);
        }
    }

    /**
     * Display Admin notices
     *
     */
    public function notices()
    {

        $classes = array(E_USER_ERROR=>'notice notice-error', 'heads-up'=>'notice notice-info is-dismissable');
        $notices = get_transient('real_search_notices_admin_error');
        if ( is_array($notices) )
        {
            foreach ($notices as $notice)
            {
                $class = isset( $classes[ $notice['flag'] ] ) ? $classes[ $notice['flag'] ] : 'notice notice-info ';
                printf( '<div class="%1$s"><p>%2$s</p></div>', $class, $notice['msg'] );
            }
        }

    }
    private function puc()
    {
        require_once $this->path.'/libs/PUC/plugin-update-checker.php';
        $update_slug   = 'realsearch';

        $puc = PucFactory::buildUpdateChecker(
            $this->api_url.'update.php?key='. urlencode(site_url()) ,
            __FILE__,
            'realsearch',
            12,
            $update_slug
        );
        add_filter('puc_request_info_result-'.$update_slug, array($this, 'puc_hook'), 10, 2);

    }

    public function puc_hook($info)
    {
        $info->download_url = str_replace('[[PARAMETERS]]', '?key=' . $this->settings['purchase_key'] . '&site=' . site_url(), $info->download_url);
        return $info;

    }

}

function rs_log($level,$data, $pre='DEBUG')
{

    if (defined('REALSEARCH_DEBUG') && REALSEARCH_DEBUG === true)
    {
        $backtrace =  (REALSEARCH_LOG_LEVEL > 11) ? debug_backtrace() : '';

        if (!is_string($data))
        {
            $data = "$pre:\n".var_export($data,true)."\n";
        }
        $debug = "\n".$backtrace."\n".date('Y-m-d H:i:s')."  [".$level.'] '.$data ."";
        if (REALSEARCH_LOG_LEVEL < 0 && REALSEARCH_LOG_LEVEL == $level) {
            error_log($debug);
            return;
        }
        if ($level < REALSEARCH_LOG_LEVEL ){
            error_log($debug);

        }

        //Use FirePHP
        if ( defined("REALSEARCH_DEBUG_FB") &&  REALSEARCH_DEBUG_FB === true )
        {
            require_once dirname(__FILE__).'/devel/FirePHPCore/fb.php';
            fb($debug);
            fb($data);
        }
    }
    require_once dirname(__FILE__).'/debug.php';
    $debug = new RealSearchDebug();
    $debug->log_error($level,$data);

}

$RealSearch = new RealSearch();

?>