<?php
/**
* Get the post (excerpt)
*
* @return void Content is directly echoed
**/
if ( ! function_exists( 'avada_render_blog_post_content' ) )
{
        function avada_render_blog_post_content() {

            if ( is_search() && ! Avada()->settings->get( 'search_excerpt' ) ) {
               return;
            }

            if ( is_search() && Avada()->settings->get( 'search_excerpt' ) ) {
                echo  apply_filters('get_the_excerpt', fusion_get_post_content() );
                return false;
            }

            echo fusion_get_post_content();
        }
}
?>