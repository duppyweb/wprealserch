<?php

namespace ZendSearch\Lucene\Document;


use ZendSearch\Lucene;
use ZendSearch\Lucene\Document\Exception\InvalidArgumentException;
use ZendSearch\Lucene\Exception\ExtensionNotLoadedException;
use ZendSearch\Lucene\Exception\RuntimeException;


Class Pdfex extends \ZendSearch\Lucene\Document{

    protected $file;
    protected $store;
    protected $parser;
    protected $odoc;

    public  $text = '';

    function  __construct($file, $store )
    {

        $this->file  = $file;
        $this->store = $store;
        $this->parser = new \Smalot\PdfParser\Parser();
        $this->get_text_from_pdf();

    }

    public static function loadPDF($file,$store=false)
    {
        rs_log(5, "Load pdf".$file);
        return new self($file, $store);
    }


    function get_text_from_pdf()
    {
        rs_log(5, "Starting PDF parsing: ".$this->text);
        try
        {
            $pdf = $this->parser->parseFile($this->file);
            $this->text =  $pdf->getText();
        }
        catch (\Exception  $e)
        {
            rs_log(5, "Failed parsing PDF: ".$e->getMessage());
        }
        rs_log(5, "Got text: ".$this->text);
    }


}


?>