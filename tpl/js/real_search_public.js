/**
* Copyright LICENSE_RS.TXT
**/
jQuery(function($){
    $(document).ready(function(){
        //Generic WP_Ajax call
        function wp_ajax(obj,callback){
            obj.action = "real_search_public_ajax";
            $.ajax({
                "url" : ajaxurl,
                "method": "POST",
                "dataType": "json",
                "success": callback,
                "data" : obj,
                "error" : ajax_error,
                "timeout" : ajax_error
            });
        }
        function ajax_error(){
            //Silently fail
        }

        //Highlight Search results recursive in sequence
        function highligh_ajax(){
            var elem = $(".rs-hl-ajax").first();
            if ( elem.data('hl-id') ){
                elem.removeClass('rs-hl-ajax');
                wp_ajax({"subaction":"highlightPost", "id":elem.data('hl-id'), "comment_id":elem.data('hl-comment-id') , "qk": elem.data('hl-qk') }, function(data){
                    if (data.success == true){
                        if (data.data){
                            elem.html(data.data);
                        }
                    } else {
                    }
                    highligh_ajax();
                })

            } else {
                //We are done, no more items, call the hook if exists
                if ( typeof  rs_ajax_hl_done === 'function' ){
                    rs_ajax_hl_done();
                }
                return false;
            }
        }
        highligh_ajax();

        //Tracking
        if (window.location.hash){
           var id =  window.location.hash.match(/rs-([0-9]+)/);
           var tracking_id = id[0];
            wp_ajax({"subaction":"track","id":tracking_id},null);
        }

    });
});