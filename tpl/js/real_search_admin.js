//Global variable
var REAL_SEARCH = {};

(function ($, undefined) {
    $.fn.getCursorPosition = function() {
        var el = $(this).get(0);
        var pos = 0;
        if('selectionStart' in el) {
            pos = el.selectionStart;
        } else if('selection' in document) {
            el.focus();
            var Sel = document.selection.createRange();
            var SelLength = document.selection.createRange().text.length;
            Sel.moveStart('character', -el.value.length);
            pos = Sel.text.length - SelLength;
        }
        return pos;
    }
})(jQuery);

jQuery(function($){
    $(document).ready(function(){
        var messages = {};
        //Generic WP_Ajax call
        function wp_ajax(obj, callback, callBackError){
            obj.action = "real_search_ajax";
            obj.token = $("#real_serach_token").val();
            if (typeof callBackError == "undefined")
            {
                callBackError = ajax_error;
            }
            $.ajax({
                "url" : ajaxurl,
                "method": "post",
                "dataType": "json",
                "success": callback,
                "data" : obj,
                "error" : callBackError,
                "timeout" : ajax_error
            });
        }

        //Get messages array for language support
        wp_ajax({"subaction":"getJsMessages"}, function(resp){
            messages = resp.data;
            //Stats page
            if (location.href.match(/RealSearchStats/)){
                stats_page();
            }
            getSettings(true);
        });

        //Get the current settings from Wordpress
        function getSettings(showok){
            wp_ajax({"subaction":"getSettings"}, function(resp){
                if (showok){
                    show_message("OK_GET_SETTINGS");
                }
                if (resp.success === true){
                    show_settings(resp.data);
                } else {
                    show_message("FAIL_SET_SETTINGS");
                }
            });
        }


        function ajax_error(){
            show_message("AJAX_ERROR");
        }

        //Display a message to the User
        function show_message(message){
            if (messages.hasOwnProperty(message)){
                message = messages[message];
            }
            $("#real_search_message").text(message);
        }

        //Display the settings, populates options etc.
        function show_settings(data){
            console.log(data);
            if (!data){
                show_message("FAIL_GET_SETTING");
            }
            //Enable the Plugin
            if (data.settings.plugin_enabled === true){
                $("#plugin_enabled").prop("checked", true);
            }
            //Enable the Plugin
            if (data.settings.comments === true){
                $("#comments").prop("checked", true);
            }
            //Locale
            if (data.settings.locale){
                $("#locale option").each(function(i,e){
                   if (data.settings.locale == $(e).val()){
                       $(e).attr('selected','true');
                   }
                });
            }

            //Ajax Search
            if (data.settings.ajax_search === true){
                $("#ajax_search").prop("checked", true);
            }

            if (data.counters == false ) {
                $('#google_counter').text('0')
            } else {
                var counter  = '';
                for (var i in data.counters ){
                    if (data.counters.hasOwnProperty(i)){
                        counter = counter + i + ' : ' + data.counters[i] + '<br>';
                    }
                }
                $('#google_counter').html(counter);
            }


            //Highlight status
            if (data.settings.highlight === true){
                $("#highlight").prop("checked", true);
                $(".highlight_html_status").show();
                //Highlight HTML
            } else {
                $(".highlight_html_status").hide();
            }


            $("#highlight_ajax").prop("checked", data.settings.highlight_ajax);
            $("#google_vision_key").val(data.settings.google_vision_key);



            $("#highlight_html").val(data.settings.hl_html);

            //Stats
            $("#real_search_count").text(data.stats.count +"/"+data.stats.docs);

            $("#post_types_disabled").empty();
            $("#post_types_enabled").empty();
            if ( data.settings.selector_text ){
                $("#editable").val(data.settings.selector_text);
            }

        }


        //Enable highlight Option
        $("#highlight").click(function(){
            if ( $("#highlight").is(":checked") ){
                $(".highlight_html_status").each(function(i,e){$(e).show()});
            } else {
                $(".highlight_html_status").each(function(i,e){$(e).hide()});
            }
        });

        //Optimize index Button click
        $("#real_search_optimize").click(function(){
            show_message("OPTIMIZING");
            wp_ajax({"subaction":"optimize"}, function(resp ){
                    if (resp.success) {
                        getSettings();
                        show_message("OK_OPTIMIZE");
                    } else {
                        show_message("FAIL_OPTIMIZE")
                    }
            });
        });

        //Clear Search cache from Transient tables
        $("#real_search_clear").click(function(){
            show_message("CLEARING");
            wp_ajax({"subaction":"clearCache"}, function(resp ){
                if (resp.success) {
                    getSettings();
                    show_message("OK_CLEAR_CACHE");
                } else {
                    show_message("FAIL_CLEAR_CACHE")
                }
            });
        });

        //Sanity check before Reindex
        function reindex_toggle(lock){
            if (lock){
                $("#real_search_reindex").prop('disabled',true);
                $("#reindex_message").text(messages["REINDEX_LOCKED"])
            } else {
                $("#real_search_reindex").prop('disabled',false);
                $("#reindex_message").text('');
            }
        }
        $('#editable').change(function(){
            reindex_toggle(true);
        });

        $('#google_vision_key').change(function(){
            reindex_toggle(true);
        });



        //Reindex the index Call
        function reindex(ids,pos){
            var step = 5;
            var total = ids.length;
            var part = ids.slice(pos,pos+step);
            var procent = 0;
            if ( pos >= total ) {
                show_message("OK_REINDEXING");
                getSettings();
                return true;
            }

            wp_ajax({"subaction":"reindexPosts", "ids":part}, function(status){
                    if (status.success !== true)
                    {
                        console.log("Error Indexing");
                        console.log(part);
                    }
                    pos = pos + step;
                    procent = (pos * 100 ) / total;
                    procent = procent.toFixed(2);
                    if (procent > 100) { procent = 100.00 }
                    show_message("REINDEXING");
                    $("#rs_bar").css({"width":procent+"%"});
                    $("#rs_label").html(procent+" %");
                    reindex(ids,pos);
            }, function(){
                if (typeof this.data !== "undefined")
                {
                    console.log("Error Indexing CODE 500 Request data: "+decodeURI(this.data));
                }

                pos = pos + step;
                procent = (pos * 100 ) / total;
                procent = procent.toFixed(2);
                if (procent > 100) { procent = 100.00 }
                show_message("REINDEXING");
                $("#rs_bar").css({"width":procent+"%"});
                $("#rs_label").html(procent+" %");
                reindex(ids,pos);
            })

        }

        //Reindex Button
        $("#real_search_reindex").click(function(){
            wp_ajax({"subaction":"getAllPostIDs"}, function(resp){
                if (resp.success === true){
                    reindex(resp.data,0);
                    $("#progress_holder").empty();
                    $("#progress_holder").html("<div id='rs_progress'><div id='rs_bar'><div id='rs_label'>0%</div></div></div>");
                } else {
                    show_message(resp.data);
                }

            })
        });

        //Drop index Button
        $("#real_search_drop_index").click(function(){
            if ( confirm(messages["CONFIRM_DROP"]) ) {
                wp_ajax({"subaction":"deleteIndex"}, function(resp){
                    if (resp.success === true){
                        show_message("OK_RESET");
                        getSettings();
                    } else {
                        shoe_message("FAIL_RESET");
                        show_message(resp.data);
                    }

                });
            }
        });

        //Save settings Button
        $("#submit").click(function(e){
            e.preventDefault();
            settings = {};
            settings.subaction = 'saveSettings';
            settings.selector_html = $('#editable').html();
            settings.selector_text = $('#editable').val();
            settings.plugin_enabled = $("#plugin_enabled").is(':checked');
            settings.comments = $("#comments").is(':checked');
            settings.ajax_search = $("#ajax_search").is(':checked');
            settings.highlight = $("#highlight").is(':checked');
            settings.hl_html = $("#highlight_html").val();
            settings.locale = $("#locale").val();
            settings.highlight_ajax = $("#highlight_ajax").is(':checked');
            settings.google_vision_key = $("#google_vision_key").val();

            settings.index_post_types = [];

            wp_ajax(settings, function(resp){
                if (resp.success === true){
                    reindex_toggle(false);
                    show_message("OK_SET_SETTINGS");
                } else {
                    show_message(resp.data);
                }
            })

        });

        //Selector in RS-QL
        wp_ajax({"subaction":"getSelectors"}, function(resp){
            if (resp.success == true) {
                //Master options
                var master = [
                    {"name":"post.author"},
                    {"name":"post.category"},
                    {"name":"post.type"},
                    {"name":"post.status"},
                    {"name":"post.id"}
                ];
                //Taxonomy
                var taxonomy = $.map(resp.data.taxonomy,function(value,i) {
                    if (resp.data.taxonomy[i].length > 0){
                        return {'name':'taxonomy.'+i };
                    }
                });

                $('.post_types').atwho({
                    data: master,
                    at: "p",
                    displayTpl: "<li>${name}</li>",
                    insertTpl:  '${name}'
                });

                $('.post_types').atwho({
                    data: taxonomy,
                    at: "t",
                    limit: 5,
                    displayTpl: "<li>${name}</li>",
                    insertTpl:  '${name}'
                });

                var glob = {};
                //Authors
                var author = $.map(resp.data.author,function(value,i) {
                    return {'name':value };
                });
                //Category
                var category = $.map(resp.data.category,function(value,i) {
                    return {'name':value.name, "id":i, "qkey":"category", "byid":value.term_id};
                });
                //Post type
                var type = $.map(resp.data.type,function(value,i) {
                    return {'name':value, "id":i, "qkey":"post_type", "byid":value};
                });
                //Post Status
                var status = $.map(resp.data.status,function(value,i) {
                    return {'name':value, "id":i, "qkey":"post_status", "byid":value};
                });

                glob.post = {};
                glob.post.author = author;
                glob.post.category = category;
                glob.post.type = type;
                glob.post.status = status;

                glob.taxonomy = resp.data.taxonomy;


                //console.log(glob);

                function _create_validator_regexp(){
                    var space = '(\\s*)';
                    var token = '(([a-z_]*)\\.([a-z_]*))';
                    var in_type = '((IN|NOT(\\s*)IN)(\\s+)(\\((\\s*)((("[^"]+")|([0-9]+))(\\s*),?(\\s*)?)+\\)))';
                    var eq_type = '((=|!=)(\\s+)(("([^"]+)")|([0-9]+)))';
                    var block = '('+ token + '(\\s+)(' + eq_type + '|' + in_type + '))';
                    var grouper = '(\\s*)?((\\(|\\))*)?(\\s*)?';
                        block = grouper + block + grouper;
                    return new RegExp('^(\\s*)?'+block+'(((AND|OR)'+block+')*)?(\\s*)?$', 'i');
                }
                var revalid = _create_validator_regexp();
                //Main validator function
                function validator(ql){
                    //Validate parentheses
                    sum = 0;
                    for ( var i = 0; i<ql.length; i++ ){
                        var char = ql.charAt(i)
                        if ( char === '(' ){
                            sum = sum + 1;
                        }
                        if (char === ')') {
                            sum = sum - 1;
                        }
                        if (sum == -1 ){
                            return false;
                        }
                    }
                    if (sum === 0) {
                        //console.log(ql);
                        return ql.match(revalid);
                    } else {
                        return false;
                    }
                }

                //NOTE If we add new globals need to update regexp

                function automplete(event){
                    var content_raw = $(this).val();
                    var pos = $(this).getCursorPosition()
                    content_raw = content_raw.replace(/[^0-9a-zA-Z_.=\(\)\s,"]/g,'');
                    content_raw = content_raw.substr(0,pos);
                    //Get the last known element till current position
                    var re = /((post|taxonomy)\.([a-z_]*))/g
                    var current_selector = null;
                    while (match = re.exec(content_raw)) {
                        current_selector = match
                    }
                    match = current_selector;
                    if (match && typeof  match[2] !== "undefined" && typeof match[3] !== "undefined"){
                        var main = match[2];
                        var sub  = match[3];
                        var _current = "".concat(main,':',sub);
                        //console.log('_current:'+_current);
                        if ( typeof  glob[main] !== "undefined" ){
                            if (main == 'taxonomy'){
                                to_load = $.map(resp.data.taxonomy[sub], function(value,i){
                                    return {"name":value.slug}
                                });
                            } else {
                                var to_load  = glob[main][sub] || null;
                            }
                        } else {
                            //console.log('Not set'+main);
                        }

                    }
                    if (to_load){
                        $('.post_types').atwho('load', '@', to_load);
                    } else {
                        $('.post_types').atwho('load', '@', []);
                    }



                    if (validator(content_raw)){
                        $('#editable').addClass("rs-syntax-ok");
                    } else {
                        $('#editable').removeClass("rs-syntax-ok");
                    }

                }
                //Clean the content editable from empty Spans and Fonts
                function clean (){
                    $(this).find('span').each(function(i,e){
                       if ($(e).is(':empty')) {
                           $(e).remove();
                       }
                    });
                    $(this).find('font').each(function(i,e){
                        if ($(e).is(':empty')) {
                            $(e).remove();
                        }
                    });
                }

                $('.post_types').keypress(function(event){  automplete.call(this,event)});
                $('.post_types').keyup(function(event){  automplete.call(this,event)});
                $('.post_types').mousedown(function(event){ automplete.call(this,event)});
                $('.post_types').change(function(event){ automplete.call(this,event)})
                //Main @ hook
                $('.post_types').atwho({
                    at: "@",
                    displayTpl: "<li>${name}</li>",
                    insertTpl: '"${name}"'
                });

            } else {
                show_message("");
            }
        });


        //Activation
        $('#activate').click(function(){
            show_message("ACTIVATING");
            $("#extra_message").text(messages['ACTIVATING']);
            var code = $("#code").val();
            var that = this;
            wp_ajax({"subaction":"activate", "code":code}, function(resp){
                if (resp.success == true){
                    $("#extra_message").text(messages['ACTIVATING_OK']);
                    show_message("ACTIVATING_OK");
                    setTimeout(function(){location.reload(true);}, 4000);
                } else {
                    $("#extra_message").text(messages['ACTIVATING_FAIL']);
                    show_message("ACTIVATING_FAIL");
                }
            });
        });

        function stats_page() {

             //Data Table language
            var dtLang =    {
                "decimal":        "",
                "emptyTable":     messages["DT_emptyTable"],
                "info":           messages["DT_info"],
                "infoEmpty":      messages["DT_infoEmpty"],
                "infoFiltered":   messages["DT_infoFiltered"],
                "infoPostFix":    "",
                "thousands":      messages["DT_thousands"],
                "lengthMenu":     messages["DT_lengthMenu"],
                "loadingRecords": messages["DT_loadingRecords"],
                "processing":     messages["DT_processing"],
                "search":         messages["DT_search"],
                "zeroRecords":    messages["DT_zeroRecords"],
                "paginate": {
                   "first":      messages["DT_paginate_first"],
                    "last":      messages["DT_paginate_last"],
                    "next":      messages["DT_paginate_next"],
                    "previous":  messages["DT_paginate_previous"]
                },
                "aria": {
                    "sortAscending":  messages["DT_aria_sortAscending"],
                    "sortDescending": messages["DT_aria_sortAscending"]
                }
            }

            $('#search_queries').DataTable({
                "language" : dtLang,
                 "processing": true,
                 "serverSide": true,
                 "order": [[ 3, "desc" ]],
                 "ajax": {
                    "url": ajaxurl,
                    "type": "POST",
                    "dataFilter" : function(resp){
                        var resp = jQuery.parseJSON( resp );
                        var response = {};
                        response.draw = resp.draw;
                        response.recordsFiltered = parseInt(resp.queryRecordCount)
                        response.recordsTotal = parseInt(resp.totalRecordCount)
                        response.data  = [];
                        for (var i = 0; i<resp.records.length; i++){
                            var elem = resp.records[i];
                            response.data.push([elem.query, elem.result, elem.clicks, elem.stamp]);
                        }
                        return JSON.stringify( response );
                    },
                    "data": {
                        "action": "real_search_ajax",
                        "token" : $("#real_serach_token").val(),
                        "subaction" : "serachQueries"
                    }
                }
            });

            if ($.cookie('real_search_period')){
                $('#period option').each(function(i,e){
                    if ( $(this).val() == $.cookie('real_search_period') ){
                        $(this).prop('selected', true);
                    }
                })
            }
            $('#period').change(function(){
                $.cookie('real_search_period', $(this).val(), {"expires":365});
                get_stats();
            });

            if ($.cookie('real_search_scale')){
                $('#scale option').each(function(i,e){
                    if ( $(this).val() == $.cookie('real_search_scale') ){
                        $(this).prop('selected', true);
                    }
                })
            }
            $('#scale').change(function(){
                $.cookie('real_search_scale', $(this).val(), {"expires":365});
                get_stats();
            });




            //Graphs
            var screenWidth;
            var screenHeight;

            var rect;
            if (self==top) {
                rect = document.body.getBoundingClientRect();
            }
            else {
                rect =  parent.document.body.getBoundingClientRect();
            }
            //Set display size based on window size.
            screenWidth = (rect.width < 960) ? Math.round(rect.width*.95) : Math.round((rect.width - 210) *.95);
            screenHeight = 600;
            REAL_SEARCH.vizuly = {};
            REAL_SEARCH.vizuly.screenWidth = screenWidth;
            REAL_SEARCH.vizuly.screenHeight = screenHeight;
            REAL_SEARCH.vizuly.size  = $('.wrap').width() - 30 + ',600';
            REAL_SEARCH.vizuly.title = 'RealSearch Stats';

            //LoadData
            function get_stats(){
                var scale = $('#scale').val();
                var period = $('#period').val();

                wp_ajax({"subaction":"getHistogram", "scale":scale, "period":period}, function(response){
                    if (response.success){
                        var data = response.data;
                        var clicks = [];
                        var searches = [];
                        for (var i = 0; i<data.length; i = i +1){
                            if (i==0 && data[0].scale){
                                //Fake data will send an extra S param in the first data point
                                REAL_SEARCH.vizuly.title = 'RealSearch Stats (Demo Data)';
                                scale = data[0].scale;
                            } else {
                                REAL_SEARCH.vizuly.title = 'RealSearch Stats';
                            }
                            var date = new Date(parseInt(data[i].slot) * scale * 1000);
                            clicks.push({"name":"Clicks","date":date, "Volume":parseInt(data[i].clicks)});
                            searches.push({"name":"Searches", "date":date, "Volume":parseInt(data[i].searches)});
                        }
                        var graphData = [searches, clicks];
                        $('#viz_container').html('');
                        // Set the size of our container element.
                        viz_container = d3.selectAll("#viz_container")
                            .style("width", screenWidth + "px")
                            .style("height", screenHeight + "px");
                        initializeGraph(graphData);
                    } else {

                    }

                });
            };

            get_stats();


            //Tag Cloud
            wp_ajax({"subaction":"tagCloud"}, function(cloud){
                if (cloud.success == true){
                    var words = cloud.data;
                    d3.wordcloud()
                        .size([$('.wrap').width() / 2 -40 , $("#wordcloud").height()])
                        .fill(d3.scale.ordinal().range(["#884400", "#448800", "#888800", "#444400"]))
                        .words(words)
                        .scale('sqrt')
                        .start();
                } else {
                    show_message("CLOUD_FAIL");
                }
            });

        }





    });
});

