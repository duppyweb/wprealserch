<?php
/**
 * https://wordpress.org/plugins/elasticsearch-indexer/
 * Credits: wallmanderco
 */

Class RealSearchUtils {


    public function prepare_post(WP_Post $post )
    {

        //Get Posts author data
        $user = get_userdata($post->post_author);
        $post_user = array();
        if ($user instanceof WP_User)
        {
            $post_user['login'] =  $user->user_login;
            $post_user['display_name'] =  $user->display_name;
            $post_user['id'] =  $user->ID;
        }
        else
        {
            $post_user['login'] =  '';
            $post_user['display_name'] =  '';
            $post_user['id'] =  '';
        }

        $post_date         = $post->post_date;
        $post_date_gmt     = $post->post_date_gmt;
        $post_modified     = $post->post_modified;
        $post_modified_gmt = $post->post_modified_gmt;

        $post_date         = ( strtotime( $post_date     ) <= 0) ? $post_date : null;
        $post_date_gmt     = ( strtotime( $post_date_gmt ) <= 0) ? $post_date_gmt : null;
        $post_modified     = ( strtotime( $post_modified ) <= 0) ? $post_modified : null;
        $post_modified_gmt = ( strtotime( $post_modified_gmt ) <= 0) ? $post_modified_gmt  : null;

        $post_args = array(
            'post_id'                  => $post->ID,
            'post_author_login'        => $post_user['login'],
            'post_author_display_name' => $post_user['display_name'],
            'post_author_id'           => $post_user['id'],
            'post_date'                => $post_date,
            //'post_date_gmt'            => $post_date_gmt,
            'post_title'               => $post->post_title,
            'post_excerpt'             => $post->post_excerpt,
            'post_content'             => $post->post_content,
            'post_status'              => $post->post_status,
            'post_name'                => $post->post_name,
            'post_modified'            => $post_modified,
            //'post_modified_gmt'        => $post_modified_gmt,
            'post_parent'              => $post->post_parent,
            'post_type'                => $post->post_type,
            'post_mime_type'           => $post->post_mime_type,
            'permalink'                => get_permalink($post->ID),
            'terms'                    => static::prepare_terms($post),
            'post_meta'                => $meta = static::prepare_meta($post),
            'alt_text'                 => static::get_alt_text($post->post_type, $post->ID)
            //'post_meta_num'            => static::prepare_meta_num($meta),
            //'post_date_object'         => static::prepare_date_terms($post_date),
            //'post_date_gmt_object'     => static::prepare_date_terms($post_date_gmt),
            //'post_modified_object'     => static::prepare_date_terms($post_modified),
            //'post_modified_gmt_object' => static::prepare_date_terms($post_modified_gmt),
            //'menu_order'               => $post->menu_order,
            //'guid'                     => $post->guid,
            //'comment_count'            => $post->comment_count,
        );
        return $post_args;

    }

    /**
     * @param $post_date_gmt
     *
     * @return array
     *
     * @author 10up/ElasticPress
     */
    protected static function prepare_date_terms( $post_date_gmt )
    {
        $timestamp  = strtotime( $post_date_gmt );
        $date_terms = array(
            'year'          => (int) date('Y', $timestamp),
            'month'         => (int) date('m', $timestamp),
            'week'          => (int) date('W', $timestamp),
            'dayofyear'     => (int) date('z', $timestamp),
            'day'           => (int) date('d', $timestamp),
            'dayofweek'     => (int) date('d', $timestamp),
            'dayofweek_iso' => (int) date('N', $timestamp),
            'hour'          => (int) date('H', $timestamp),
            'minute'        => (int) date('i', $timestamp),
            'second'        => (int) date('s', $timestamp),
            'm'             => (int) (date('Y', $timestamp).date('m', $timestamp)), // yearmonth
        );

        return $date_terms;
    }


    /**
     * @param $post
     *
     * @return array
     */
    protected static function prepare_terms( $post )
    {
        $taxonomies = get_object_taxonomies( $post->post_type, 'objects' );
        $terms      = array();

        foreach ($taxonomies as $taxonomy) {
            $objectTerms = get_the_terms( $post->ID, $taxonomy->name );

            if ( is_wp_error( $objectTerms ) )
            {
                continue;
            }

            if (!$objectTerms)
            {
                $terms[$taxonomy->name] = [];
                continue;
            }

            foreach ($objectTerms as $term)
            {
                $allSlugs = array($term->slug);

                //Add parent slug
                if ($parent = get_term_by('id', $term->parent, $term->taxonomy))
                {
                    $allSlugs[] = $parent->slug;
                }

                $terms[$term->taxonomy][] = array(
                    'term_id'   => $term->term_id,
                    'slug'      => $term->slug,
                    'name'      => $term->name,
                    'parent'    => $term->parent,
                    'all_slugs' => $allSlugs,
                );
            }
        }

        return $terms;
    }

    /**
     * @param $post
     *
     * @return array
     */
    public static function prepare_meta($post)
    {
        $meta = update_meta_cache('post', [$post->ID])[$post->ID];

        if (empty($meta))
        {
            return array();
        }

        return array_map('maybe_unserialize', $meta);
    }

    /**
     * @param array $meta
     *
     * @return array
     */
    public static function prepare_meta_num(array $meta)
    {
        foreach ($meta as $key => $value)
        {
            $meta[$key] = array_map('intval', $value);
        }

        return $meta;
    }

    /**
     * Preapres comment for indexing
     * @param $comment
     */
    public function prepare_comment($comment)
    {
        $comment = (array)$comment;
        return $comment;
    }

    public static function get_alt_text($type, $id)
    {
        if ($type == 'attachment')
        {

            try
            {
                //Hack to get the Alt text
                $alt = wp_get_attachment_image($id);
                preg_match('~alt="([^"]+)"~', $alt, $mat);
                if ( isset($mat[1]))
                {
                    return $mat[1];
                }
            }
            catch (Exception $e)
            {
                return '';
            }
            return '';

        }

    }
    public static function authors()
    {
        global $wpdb;
        $authors_sql =  (array)$wpdb->get_results( "SELECT DISTINCT `post_author`,{$wpdb->users}.user_nicename FROM {$wpdb->posts} INNER JOIN {$wpdb->users} ON {$wpdb->users}.ID = {$wpdb->posts}.post_author");
        $authors = array();
        foreach ($authors_sql as $author)
        {
            $authors[  $author->post_author ] = RealSearchUtils::clean($author->user_nicename);
        }
        return $authors;
    }

    public static function statuses()
    {
        $satuses = get_post_stati();
        $satuses[] = 'inherit';
        return $satuses;
    }

    public static function taxonomy()
    {
        $data = array();
        //Taxonomies
        $taxonomies = get_taxonomies(array());
        foreach ( $taxonomies as $key=>$value )
        {
            $terms = get_terms( $key, array(
                'hide_empty' => false,
            ) );
            $taxonomies[$key] = $terms;
        }
        return $taxonomies;

    }

    public static function sort_by_kye($data, $sortKey, $sort_flags=SORT_DESC)
    {
        if (empty($data) or empty($sortKey)) return $data;
        $ordered = array();
        foreach ($data as $key => $value)
            $ordered[$value[$sortKey]] = $value;
        ksort($ordered, $sort_flags);
        return array_values($ordered);
    }

    public static function regexp_explode($mat)
    {
        if ( isset($mat[0]) )
        {
            $re = array();
            foreach ($mat as $index=>$block){
                $i = 0;
                foreach ($block as $key=>$value)
                {
                    $re[$key][$index]  = $value;
                }
            }
            return $re;
        }
        else
        {
            return array();
        }
    }

    public  static function clean($string)
    {
        rs_log(0, $string);
        $string = preg_replace('~(\r|\n)~s', ' ',  $string);
        $string = preg_replace('~[^0-9a-zA-Z_.=\(\)\,"\s!]~i', '',  $string);
        return $string;
    }


}