<?php
Class RealSearchVision
{
    public  $post = null;
    private $key = null;
    private $path = null;
    private $image = null;
    private $table = 'rs_vision_cache';
    private $option_key = 'real_search_counter';
    private $use_cache  = true;
    private $active = false;

    public function __construct($settings, $path, $api_url)
    {
        global $wpdb;
        //Check for KEY and Status
        $this->path = $path;
        $this->table = $wpdb->prefix . $this->table;
        if ( is_array($settings) )
        {
            if ( trim(strtolower($settings['google_vision_key'])) == '') {
                $this->active = false;
            }
            else
            {
                $this->key = trim($settings['google_vision_key']);
                $this->url = 'https://vision.googleapis.com/v1/images:annotate?key='.$this->key;
                $this->active = true;
            }
        }
    }

    /**
     * Does Image analysis OCR and LABEL detection via Google Cloud Vision API
     * Returns false on failure
     * @return bool|string
     */
    public function vision()
    {
        global $RealSearch;
        if ($this->active == false)
        {
            return "GOOGLEVISIONINACTIVE";
        }
        $this->image = @file_get_contents($this->path);
        if ( false !== ( $text = $this->get_cache() ) )
        {
            rs_log(5, "Google Vision API, cached response.");
            return $text;
        }
        // Do the post to Google
        $data = array(
            'requests' => array(
                'image'    => array( 'content'=> base64_encode( $this->image ) ) ,
                'features' => array(
                                array( 'type'=>'TEXT_DETECTION',  'maxResults'=>3 ),
                                array( 'type'=>'LABEL_DETECTION', 'maxResults'=>10 ),
                              )
            )
        );
        $post = json_encode($data);
        rs_log(3,"Google Vision API Sending...");
        $this->increase_counter($data);
        $response_raw = wp_remote_post($this->url, array( "method"=>"POST", 'body'=>$post, 'timeout'=>10, 'headers'=>('Content-Type: application/json') ) );
        rs_log(3,"Google Vision API Sent!");
        $labels = array();
        $ocr = array();
        rs_log(5,$response_raw);
        if ( is_wp_error( $response_raw ) )
        {
            rs_log(1,"Google Vision API error:".$response_raw->get_error_message());
            return false;
        }
        else
        {
            //We got the response from the API
            $response = json_decode($response_raw['body'],true);
            if ( !isset($response['responses']) )
            {
                rs_log(1,"Google Vision API error: No response in JSON");
                return false;
            }
            else
            {

                foreach ( @$response['responses'][0]['labelAnnotations'] as $label )
                {
                    rs_log(7,'labelAnnotations: '.$label['description']);
                    $labels[] = $label['description'];
                }

                foreach ( @$response['responses'][0]['textAnnotations'] as $label )
                {
                    rs_log(7,'textAnnotations: '.$label['description']);
                    $ocr[] = $label['description'];
                }

            }

        }
        $text =  join(" ",$labels)." ".join(" ",$ocr);
        rs_log(7,$text,"Google Vision Text");
        $this->save_cache($text, $response_raw);
        return $text;

    }

    /**
     * Gets image text from cache to avoid extra cost doing re-indexing.
     * @return bool|string
     */
    private function get_cache()
    {
        if (!$this->use_cache)
        {
            return false;
        }
        global $wpdb;
        $md5 = md5_file($this->path);
        $cache = $wpdb->get_row( $wpdb->prepare( "SELECT image_text FROM {$this->table} WHERE md5 = %s LIMIT 1", array($md5) ) );
        return (isset($cache->image_text)) ? $cache->image_text : false;
    }
    /*
     * Sets image text to caches
     */
    private function save_cache($text, $response)
    {
        if (!$this->use_cache)
        {
            return false;
        }
        global $wpdb;
        $md5 = md5_file($this->path);
        $text = (string)$text;
        $raw_response = base64_encode(serialize($response));
        $wpdb->insert($this->table, array('md5'=>$md5, 'image_text'=>$text, 'raw_response'=>$raw_response));

    }

    /**
     * Tests Google Cloud Vision API key
     *
     */
    public function test_auth()
    {
        $this->path = dirname(__FILE__).'/tpl/auth.png';
        $this->use_cache = false;
        $text = $this->vision();
        preg_match( '~Real(\s*)Search(\s*)API~isu ',$text, $mat);
        return isset($mat[0]);
    }


    /**
     * Increase Counters
     */
    public function increase_counter($data)
    {
        $counters = get_option($this->option_key);
        if ( isset( $data['requests']['features'] ) )
        {
            foreach( $data['requests']['features'] as $feature )
            {
                $counters[$feature['type']] = isset( $counters[$feature['type']] ) ? $counters[$feature['type']] + 1 : 1;
            }
        }
        update_option($this->option_key, $counters);

    }

    public function get_counter()
    {
        return get_option($this->option_key);
    }
}
?>