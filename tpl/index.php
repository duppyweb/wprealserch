<div class="wrap">
    <h1><?php esc_html_e('RealSearch settings', 'real_search') ?> <span><?php esc_html_e('version', 'real_search') ?>: <?php echo $this->version; ?></span></h1>
    
    <table class="form-table">
    <form id="real_search_settings" name="real_search_settings">
        <tbody>
            <tr>
                <th scope="row"><?php esc_html_e('RealSearch Status', 'real_search') ?></th>
                <td>
                    <fieldset>
	                    <input name="plugin_enabled" type="checkbox" id="plugin_enabled" value="1">
                        <label for="plugin_enabled"></label>
                        <p  id="extra_message" class="description"><?php esc_html_e('Enable or disable the RealSearch plugin.', 'real_search') ?></p>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <th scope="row"><?php esc_html_e('Lanuage', 'real_search') ?></th>
                <td>
                    <fieldset id="post_types_select">
                        <select name="locale" id="locale">
                            <option value="enEN"><?php esc_html_e('English', 'real_search') ?></option>
                            <option value="deDE"><?php esc_html_e('German', 'real_search') ?></option>
                            <option value="esES"><?php esc_html_e('Spanish', 'real_search') ?></option>
                        </select>
                    </fieldset>
                    <p class="description"><?php esc_html_e('Select your site language', 'real_search') ?>
                </td>
            </tr>
            <tr>
                <th scope="row"><?php esc_html_e('Post selector', 'real_search') ?></th>
                <td>
                    <fieldset id="post_types_select">
                        <!--<div id="editable" class="post_types" contentEditable="true">
                        </div>-->
                        <textarea id="editable" class="post_types" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false"  ></textarea>

                    </fieldset>
                    <p class="description"><?php esc_html_e('Start typing, start with: post. or taxonomy. then continue with =, !=, IN (...), NOT IN (...), if you need autocomplete press [SPACE]@', 'real_search') ?>
                </td>
            </tr>
            <tr>
                <th scope="row"><?php esc_html_e('Index comments', 'real_search') ?></th>
                <td>
                    <fieldset>
                        <input name="comments" type="checkbox" id="comments" value="1">
                        <label for="comments"></label>
                        <p class="description"><?php esc_html_e('Should RealSearch index comment content?', 'real_search') ?>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <th scope="row"><?php esc_html_e('Highlight', 'real_search') ?></th>
                <td>
                    <fieldset>
	                    <input name="highlight" type="checkbox" id="highlight" value="1">
                        <label for="highlight"></label>
                        <p class="description"><?php esc_html_e('Highlighting can slightly slow down your search results', 'real_search') ?>
                    </fieldset>
                </td>
            </tr>
            <tr class="highlight_html_status">
                <th scope="row"><?php esc_html_e('Highlight HTML', 'real_search') ?></th>
                <td>
                    <fieldset>
                        <input name="hl_html" id="highlight_html" type="text" value="<span style='color:red'>[WORD]</span>" class="regular-text code">
                        <p class="description"><?php esc_html_e('Use [WORD] for matched word', 'real_search') ?></p>
                    </fieldset>
                </td>
            </tr>
            <tr class="highlight_html_status">
                <th scope="row"><?php esc_html_e('Highlight AJAX?', 'real_search') ?></th>
                <td>
                    <fieldset>
                        <input name="highlight_ajax" type="checkbox" id="highlight_ajax" value="1">
                        <label for="highlight_ajax"></label>
                        <p class="description"><?php esc_html_e('Highlighting in parallel with AJAX will be faster.', 'real_search') ?>
                    </fieldset>
                </td>
            </tr>
            <tr class="">
                <th scope="row"><?php esc_html_e('Enable Live Search?', 'real_search') ?></th>
                <td>
                    <fieldset>
	                    <input name="ajax_search" type="checkbox" id="ajax_search" value="1">
                        <label for="ajax_search"></label>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <th scope="row"><?php esc_html_e('Google Vision API Key', 'real_search') ?></th>
                <td>
                    <fieldset>
                        <input name="google_vision_key" id="google_vision_key" type="text" value="" class="regular-text code">
                        <p class="description"><?php esc_html_e('Enter your Google Cloud Vision API Key. Get your key at https://cloud.google.com/vision/', 'real_search') ?></p>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <th scope="row"><?php esc_html_e('Google Vision API Usage', 'real_search') ?></th>
                <td>
                    <p id="google_counter"></p>
                </td>
            </tr>

            <tr>
                <th scope="row"><?php esc_html_e('Total indexed posts', 'real_search') ?></th>
                <td>
                    <fieldset>
                        <span id="real_search_count">NA</span>
                        <p class="description"><?php esc_html_e('Count / Indexed documents, if the two are far apart you should Optimize', 'real_search') ?></p>
                    <fieldset>
                </td>
            </tr>
            <tr>
                <th scope="row"><?php esc_html_e('Empty/Truncate Index', 'real_search') ?></th>
                <td>
                    <fieldset>
                        <input type="button" id="real_search_drop_index" class="button button-primary" value="<?php esc_attr_e('Drop index', 'real_search') ?>">
                    </fieldset>
                </td>
            </tr>
            <tr>
                <th scope="row"><?php esc_html_e('Rebuild index', 'real_search') ?></th>
                <td>
                    <fieldset id="progress_holder">
                        <input type="button" id="real_search_reindex" class="button button-primary" value="<?php esc_attr_e('Reindex', 'real_search') ?>">
                        <span id="reindex_message"></span>
                    </fieldset>
                </td>
            </tr>
            <tr>
                <th scope="row"><?php esc_html_e('Optimize index', 'real_search') ?></th>
                <td>
                    <fieldset id="progress_holder">
                        <input type="button" id="real_search_optimize" class="button button-primary" value="<?php esc_attr_e('Optimize', 'real_search') ?>">
                    </fieldset>
                </td>
            </tr>
            <tr>
                <th scope="row"><?php esc_html_e('Clear search cache', 'real_search') ?></th>
                <td>
                    <fieldset id="progress_holder">
                        <input type="button" id="real_search_clear" class="button button-primary" value="<?php esc_attr_e('Clear', 'real_search') ?>">
                    </fieldset>
                </td>
            </tr>
            <tr>
                <th scope="row"><?php esc_html_e('Server Message', 'real_search') ?></th>
                <td>
                   <span id="real_search_message"> ... </span>
                </td>
            </tr>
            <tr>
                <th scope="row"></th>
                <td>
                   <p class="submit"><input type="submit" name="submit" id="submit" class="button button-primary" value="<?php esc_attr_e('Save Settings', 'real_search') ?>"></p>
                </td>
            </tr>
        </tbody>
        <input type="hidden" name="real_serach_token" id="real_serach_token" value="<?php echo  wp_create_nonce( 'real_search_admin' ); ?>">
    </form>
    </table>
    
</div>