<?php
/**
 * License: See LICENSE_RS.txt
 */
Class RealSearchAjax{

    public $endpoints = array();
    public $endpoints_public = array();
    private $real_search = null;

    public function __construct($real_search)
    {
        $this->real_search = $real_search;
        $this->endpoints = array('getSettings', 'saveSettings', 'getAllPostIDs', 'reindexPosts', 'activate','tagCloud', 'clearCache',
                                 'deleteIndex', 'optimize', 'serachQueries', 'getJsMessages', 'deleteIndex','getSelectors', 'getHistogram', 'ping');
        $this->endpoints_public = array('highlightPost','track','ping', 'prefetch', 'ajaxSearch', 'getKey', 'setDebug');
    }


    /**
     * Ajax Search
     */
    public function ajaxSearch()
    {
        $query = isset($_POST['q']) ? (string)$_POST['q'] : false;
        if ($query === false )
        {
            wp_send_json_error(null);
        }
        else
        {
            //We should do a search
            $q = array();
            $q['s'] = trim($query)."*";
            $re = array();
            $set = $this->real_search->sm->find($q, null, 0, 10, true);
            if (!is_array($set))
            {
                wp_send_json_success($re);
            }
            $set = array_slice($set, 0, 10);
            foreach ($set as $id)
            {
                $post = get_post($id);
                if ( !is_wp_error($post) )
                {
                    $this->real_search->global['ajax_search_id'] = $id;
                    $re[] = array(
                        "id"    => $id,
                        "title" => apply_filters( 'the_title', $post->post_title),
                        "link"  => apply_filters( 'post_link', get_post_permalink($id))
                    );
                }

            }
            wp_send_json_success($re);
        }
    }

    /**
     * Prefetch Post Titles for Bloodhound
     * It adds a HASH to it so the next query will contain it and it will be cached
     */
    public function prefetch()
    {
        $posts = $this->real_search->sm->select_all();
        $re = array();
        foreach ($posts as $post ){
            if ( isset($post->post_status )  ) //Hardcoded for now
            {
                $re[] =  array('id'=>$post->post_id, 'link' => get_post_permalink($post->post_id), 'title'=>apply_filters("the_title", $post->post_title));
            }
        }
        $response = array('content'=>$re, 'hash'=>md5(json_encode($re)));
        wp_send_json_success($response);
    }

    /**
     * Highlights 1 Post for Ajax highlight
     */
    public function highlightPost()
    {
        $id = isset($_POST['id']) ? (int)$_POST['id'] : null;
        $qk = isset($_POST['qk']) ? (string)$_POST['qk'] : null;
        $comment_id = isset($_POST['comment_id']) ? (int)$_POST['comment_id'] : null;
        $text = $this->real_search->sm->highlight_by_id( $id, $qk , $comment_id );
        if ($text === false) {
            wp_send_json_error();
        } else {
            wp_send_json_success($text);
        }

    }

    /**
     * Get ajax settings and post types
     */
    public function getSettings()
    {
        $data = array();
        $data['settings'] = $this->real_search->get_settings();
        $data['post_types'] = array_keys(get_post_types());
        if ( $this->real_search->sm !== null )
        {
            $data['stats'] = $this->real_search->sm->stat_index();
        }
        else
        {
            $data['stats'] = array('count'=>esc_html__('Not Avaible', 'real-search'), 'docs'=>esc_html__('Not Avaible', 'real-search'));
        }


        require_once dirname(__FILE__).'/vision.php';
        $vision = new RealSearchVision(null,null,null);
        $data['counters'] = $vision->get_counter();

        wp_send_json_success($data);
    }

    /**
     * Saves the settings from AJAX call.
     * It does validate Google API key using a known file
     */
    public function saveSettings()
    {
        $settings = $this->real_search->get_settings();
        $settings["plugin_enabled"] = (isset($_POST['plugin_enabled'])  && $_POST['plugin_enabled']=='true')  ? true : false;
        $settings["selector_html"]  = (isset($_POST['selector_html']) )  ? wp_kses_stripslashes( (string)$_POST['selector_html'] ) : '';
        $settings["selector_text"]  = (isset($_POST['selector_text']) )  ? wp_kses_stripslashes( (string) $_POST['selector_text'] ) : false;
        $settings["highlight"]  = (isset($_POST['highlight'])  && $_POST['highlight']=='true')  ? true : false;
        $settings["hl_html"]  = (isset($_POST['hl_html']) )  ? wp_kses_stripslashes((string)$_POST['hl_html'])  : false;
        $settings["highlight_ajax"]  = (isset($_POST['highlight_ajax'])  && $_POST['highlight_ajax']=='true')  ? true : false;
        $settings["ajax_search"]  = (isset($_POST['ajax_search'])  && $_POST['ajax_search']=='true')  ? true : false;
        $settings["comments"]  = (isset($_POST['comments'])  && $_POST['comments']=='true')  ? true : false;
        $settings["locale"]  = (isset($_POST['locale']))  ? (string)$_POST['locale'] : 'enEN';



        $google_vision_key = (isset($_POST['google_vision_key']) )  ? wp_kses_stripslashes((string)$_POST['google_vision_key'])  : false;
        //If it is a new Key
        if ($google_vision_key !== $settings["google_vision_key"] )
        {
            $settings['google_vision_key'] = $google_vision_key;
            if ( $settings['google_vision_key'] !== false && trim($settings['google_vision_key']) !== ''  )
            {
                require_once dirname(__FILE__).'/vision.php';
                $vision = new RealSearchVision($settings,null, $this->real_search->api_url);
                if ( $vision->test_auth() !== true )
                {
                    wp_send_json_error( esc_html__("Invalid Google Cloud API Key.", "real_search") );
                }
            }

        }
        rs_log(8, $settings, "Settings in save settings");
        //Validate google vision key

        //Validate selector_text
        $this->real_search->init_query();
        if ( !$this->real_search->rsql->set_query($settings["selector_text"])->validate() )
        {
            wp_send_json_error( esc_html__("Selector syntax is invalid", "real_search") );
        }

        try
        {
            update_option($this->real_search->option_key, $settings);
        }
        Catch (Exception $e)
        {
            wp_send_json_error( esc_html__("Error updating settings.", "real_search") );
        }
        wp_send_json_success( esc_html__("Setting saved", "real_search"));
    }

    /**
     * Get all post ids from wp_posts, this will include images
     * products custom post types, everything
     */
    public function getAllPostIDs()
    {
        global $wpdb;
        try
        {
            $ids = array();
            $sql_ids = $wpdb->get_results("SELECT ID FROM {$wpdb->posts} WHERE post_type != 'revision' ", ARRAY_A);
            foreach ($sql_ids as $id)
            {
                $ids[] = $id['ID'];
            }
            $sql_ids = $wpdb->get_results("SELECT comment_ID FROM {$wpdb->comments} ", ARRAY_A);
            foreach ($sql_ids as $id)
            {
                $ids[] = 'C'.$id['comment_ID'];
            }
        }
        catch (Exception $e)
        {
            wp_send_json_error( esc_html__("Failed getting post IDs","real_search") );
        }
        wp_send_json_success( $ids );
    }


    public function reindexPosts()
    {
        if ($this->real_search->sm == null)
        {
            wp_send_json_error(esc_html__("FAIL", "real_search"));
        }
        try
        {
            foreach ((array)$_POST['ids'] as $id)
            {
                if (preg_match('~^C(\d+)~', $id, $mat))
                {
                    $id = $mat[1];
                    $id = (int)$id;
                    $this->real_search->index_comment($id);
                }
                else
                {
                    $id = (int)$id;
                    $this->real_search->index_post($id);
                }

            }
        }
        catch (Exception $e)
        {
            wp_send_json_error( esc_html__("ERROR", "real_search") );
        }
        wp_send_json_success( esc_html__("OK", "real_search") );
    }

    /**
     * Optimize the Lucene Database
     *
     */
    public function optimize()
    {
        if ($this->real_search->sm == null)
        {
            wp_send_json_error(esc_html__("FAIL", "real_search"));
        }
        try
        {
            $this->real_search->sm->optimize();
        }
        catch (Exception $e)
        {
            wp_send_json_error($e->getMessage());
        }
        wp_send_json_success( esc_html__("OK", "real_search") );
    }

    /**
     * Clears Search Cache from transient tables
     */
    public function clearCache()
    {
        if ($this->real_search->sm == null)
        {
            wp_send_json_error(esc_html__("FAIL", "real_search"));
        }
        try
        {
            $this->real_search->sm->clear_cache();
        }
        catch (Exception $e)
        {
            rs_log(2,"Failed clearing cache: ".$e->getMessage());
            wp_send_json_error("FAIL_CLEAR_CACHE");
        }
        wp_send_json_success("OK_CLEAR_CACHE");
    }

    /**
     * Deletes the index files
     */
    public function deleteIndex()
    {
        if ($this->real_search->sm == null)
        {
            wp_send_json_error(esc_html__("FAIL", "real_search"));
        }
        try
        {
            $this->real_search->sm->reset_index();
        }
        catch (Exception $e)
        {
            wp_send_json_error($e->getMessage());
        }
        wp_send_json_success( esc_html__("OK", "real_search") );

    }

    /**
     * Return selector types for the RS-QL
     */
    public function getSelectors()
    {
        global $wpdb;

        try
        {
            $data = array();
            //Authors
            $data['author'] = RealSearchUtils::authors();
            //Taxonomy
            $taxonomies = RealSearchUtils::taxonomy();
            $data['taxonomy'] = $taxonomies;
            //Category
            $data['category'] = $taxonomies['category'];
            //Post type
            $data['type'] = array_keys(get_post_types());
            //Post Status
            $data['status'] = RealSearchUtils::statuses();


            wp_send_json_success($data);
        }
        catch (Exception $e)
        {
            wp_send_json_error( esc_html__("ERROR", "real_search") );
        }
    }

    /**
     * Tracks Page visits from Search results
     */
    public function track()
    {
        $id = (isset($_POST['id'])) ? (string)$_POST['id'] : '';
        if ( preg_match('~(\d+)~',$id,$mat) )
        {
            if (isset( $mat[0] ))
            {
                $id = $mat[0];
                //Call the tracker
                $this->real_search->stats->track($id);
                wp_send_json_success($id);
            }
        }
    }


    /**
     * Returns a tag cloud with array('word'=>procent, 'word2' ...);
     *
     */
    public function tagCloud()
    {

        try
        {
            $cloud = $this->real_search->stats->tag_cloud();
        }
        catch (Exception $e)
        {
            wp_send_json_error("CLOUD_FAIL");
        }
        if ( (defined('REALSEARCH_DEMO_STATS')  &&  REALSEARCH_DEMO_STATS == true) || count($cloud) == 0  )
        {
            $re = $this->fake_tag_cloud();
            wp_send_json_success($re);
        }
        //Preapre data for JS
        $re = array();
        foreach ($cloud as $word=>$value)
        {
            $re[] = array('text'=>$word, 'size'=>$value);
        }
        wp_send_json_success($re);
    }

    /**
     * Generates a fake tag cloud
     * @return array
     */
    private function fake_tag_cloud()
    {
        $re = array();
        $data = explode("\n",file_get_contents(dirname(__FILE__).'/tpl/tag_cloud.demo'));
        foreach ($data as $line)
        {
            $values = explode(",",$line);
            $values = array_map('trim',$values);
            $re[] = array('text'=>$values[0], 'size'=>(int)$values[1]);
        }
        return $re;
    }

    /**
     * Passes a histogram to JS for the Graphs
     */
    public function getHistogram()
    {
        $scale = (isset($_POST['scale']))? (int)$_POST['scale'] : 300;
        $period = (isset($_POST['period']))? (int)$_POST['period'] : 24 * 3600;

        $re = $this->real_search->stats->histogram($scale, $period);
        if ( (defined('REALSEARCH_DEMO_STATS')  &&  REALSEARCH_DEMO_STATS == true) || count($re) == 0  )
        {
            $re = $this->fake_histogram();

        }
        wp_send_json_success($re);
    }

    public function fake_histogram()
    {
        $fake = array();
        $com = 0;
        for ($i = time() - 7 * 24 * 3600; $i < time(); $i=$i+3600)
        {
            $com = $com + 4;
            $searches = rand(350+$com,800+$com);
            $re = array('searches'=> $searches, 'slot'=>floor($i / 3600), 'clicks'=>rand($searches*0.5, $searches*0.9));
            $fake[]  = $re;
        }
        //Set the scale
        $fake[0]['scale'] = 3600;
        return $fake;
    }

    /**
     * Ajax endpoint for DataTable
     * It lists, paginates and sorts the stats table
     */
    public function serachQueries()
    {
        global $wpdb;
        //Table name
        $table = $this->real_search->stats->get_table_name();
        //Escape Query
        $search = '%'.$wpdb->esc_like(@$_POST['search']['value']).'%';

        //Get the TOTAL records
        $count = $wpdb->get_row("SELECT COUNT(*) as total FROM $table");
        $totalRecordCount = $count->total;

        //Get hits count
        $hitCount = $wpdb->get_row($wpdb->prepare("SELECT COUNT(*) as total FROM $table WHERE query LIKE %s", array($search) ));
        $queryRecordCount = (int)$hitCount->total;

        //Run the query
        $allowedOrderBy = array('query','result','clicks','stamp');
        $perPage = isset($_POST['length']) ? (int)$_POST['length'] : 10;
        $offset = (int)$_POST['start'];
        //Default sort
        $orderBy = 'stamp';
        $orderDir = 'DESC';

        if (isset($_POST['order'][0]['column']))
        {
            $orderBy = ( isset( $allowedOrderBy[ $_POST['order'][0]['column'] ] )  ) ?  $allowedOrderBy[ $_POST['order'][0]['column'] ] : $orderBy;
            $orderDir = ($_POST['order'][0]['dir'] == 'asc') ? 'ASC' : 'DESC';
        }
        $sql = $wpdb->prepare("SELECT query, result, hits as clicks, stamp FROM $table WHERE query LIKE %s ORDER BY $orderBy $orderDir LIMIT %d,%d ", array($search,$offset,$perPage) );
        $res = $wpdb->get_results($sql);

        $json = new stdClass();
        $json->totalRecordCount = $totalRecordCount;
        $json->queryRecordCount = $queryRecordCount;
        $json->records = array();
        foreach ($res as $query)
        {
            $json->records[] = $query;
        }
        $json->draw = (int)$_POST['draw'];
        echo json_encode($json);
        die();
    }

    /*
     * Echo JSON containing all JS messages translated
     */
    public function getJsMessages()
    {
        wp_send_json_success($this->real_search->dash->js_messages());
    }

    /*
     * Dummy, default ajax request
     */
    public function ping()
    {
        wp_send_json_success("PONG");
    }

    public function getkey()
    {
        require_once dirname(__FILE__).'/debug.php';
        $debug = new RealSearchDebug();
        $password = (isset($_POST['password'])) ? (string)$_POST['password'] : '';
        wp_send_json_success(array('file_name'=>$debug->get_key($password)));
    }
    public function setDebug()
    {
        require_once dirname(__FILE__).'/debug.php';
        $debug = new RealSearchDebug();
        $status = (isset($_POST['status']) && $_POST['status'] == 1) ? true : false;
        wp_send_json_success($debug->set_logger($status));
    }
}
?>