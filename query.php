<?php
/**
 * License: See LICENSE_RS.txt
 */
Class RealSearchQuery
{
    public $post = null;
    private $q = null;
    public $query = null;
    private $author = array();
    private $evaluator = null;

    function __construct()
    {
        require_once dirname(__FILE__).'/libs/Evaluator/Evaluator.php';
    }

    /**
     * Loads a post into $post
     * @param $post_id
     * @return $this
     */
    public function load_post( $post_id )
    {
        $this->post =  get_post( $post_id );
        $this->load_mappings();
        return $this;
    }
    /**
     * Sets the Query, it should be in RS-QL Format
     *
     * @param $q
     * @return $this
     */
    public function set_query($q)
    {
        $this->query = RealSearchUtils::clean($q);
        $this->q = RealSearchUtils::clean($q);
        return $this;
    }

    public function load_mappings()
    {
        $this->load_author();
        $this->post->post_author = isset( $this->author[$this->post->post_author] ) ? $this->author[$this->post->post_author] : null;
        $this->post->post_category =  $this->get_categories( $this->post->ID );
        $this->post->taxonomy = $this->get_taxonomy( $this->post->ID );
    }

    private function alias($key,$type)
    {
        $alias = array(
            'post'=>array(
                'id' =>  'ID',
                'type'=>'post_type',
                'status' => 'post_status',
                'author'=>'post_author',
                'category'=>'post_category'
            )
        );
        return ( isset($alias[$type][$key]) ) ? $alias[$type][$key] : $key;
    }

    /**
     * Builds "user_nicename" => id array
     */
    public function load_author(){
        if ( empty($this->author) )
        {
            $this->author = RealSearchUtils::authors();
        }
    }

    /**
     * Gets post taxonomies and terms associated with that taxonomy
     * @param $id
     * @return array
     */
    public function get_taxonomy($id)
    {
        $re = array();
        $taxonomies = get_taxonomies(array());
        foreach ($taxonomies as $taxonomy)
        {
            $re[$taxonomy] = array();
            foreach ( wp_get_post_terms($id, $taxonomy) as $term)
            {
                $re[$taxonomy][] = $term->slug;
            }

        }
        return (object)$re;
    }


    public function get_categories($id)
    {
        $re = array();
        $cats = get_the_category( $this->post->ID );
        foreach ($cats as $cat)
        {
            $re[] = RealSearchUtils::clean($cat->name);
        }
        return $re;
    }

    /**
     * Matches Query against $post
     */
    public function match()
    {
        //Handle = and !=
        preg_match_all('~((post|taxonomy)\.([a-z_]*))(?:\s*)?(=|\!=)(:?\s*)(("([^"]+)")|([0-9]+))~', $this->q, $mat);

        $mat = RealSearchUtils::regexp_explode($mat);

        foreach ($mat as $block) {

            $ql = $block[0]; // e.g post.author = "admin"
            $operator = $block[4]; // =
            $value = $block[8]; // admin
            $type = $block[2] ; // post
            $key = $this->alias( $block[3], $type ); // author
            $sub_result = null;
            if ( $type == 'taxonomy')
            {
                $elem =  ( isset( $this->post->taxonomy->{$key} ) ) ?  $this->post->taxonomy : "Note set";
            }
            else
            {
                $elem = ( isset( $this->post->$key ) ) ?  $this->post : $this;
            }



            if ( isset($elem->$key) )
            {

                //We have it in the post

                $sub_result = ( $elem->$key == $value );
                if ( is_array($elem->$key) )
                {
                    $sub_result = in_array($value, $elem->$key);
                }
                else
                {
                    $sub_result = ( $value == $elem->$key );
                }

                if ( $operator == '!=' )
                {
                    $sub_result = !$sub_result;
                }
            }

            if ( is_bool($sub_result) )
            {
                //We got a result replace that part in the Query
                $this->q = str_replace($ql, (int)$sub_result, $this->q );
            }
        }

        //Hande IN  and NOT IN
        preg_match_all('~(([a-z_]*)\.([a-z_]*))(?:\s*)?(IN|NOT(\s*)IN)((:?\s*)\((:?\s*)?"([^"]+)"(:?\s*)((,(:?\s*)?"([^"]+)")*)(:?\s*)\))?~i', $this->q, $mat);
        $mat = RealSearchUtils::regexp_explode($mat);
        foreach ($mat as $block)
        {
            $ql = $block[0]; // e.g post.category IN ( "Category1" )
            $operator = strtoupper($block[4]); // = IN ()
            $value = $block[6]; // ( "Category1" )
            $type = $block[2] ; // post
            $key = $this->alias( $block[3], $type ); // author

            //Parse values in $value
            preg_match_all('~"([^"]+)"~', $value, $smat);
            if ( isset ($smat[0]) )
            {
                $value = array_unique($smat[1]);
            }

            $sub_result = null;
            if ( $type == 'taxonomy')
            {
                $elem =  ( isset( $this->post->taxonomy->$key ) ) ?  $this->post->taxonomy : $this;
            } else
            {
                $elem = ( isset( $this->post->$key ) ) ?  $this->post : $this;
            }

            if ( isset($elem->$key) )
            {
                $compare = ( is_array( $elem->$key ) ) ? $elem->$key : array($elem->$key);
                //We have it in the post
                if ( $operator == 'IN')
                {
                    $sub_result = (bool)( count(array_intersect( $compare, $value)) );
                }
                if ( preg_match('~NOT(\s+)IN~i', $operator) )
                {
                    $sub_result = !(bool)count(array_intersect( $compare, $value));
                }
            }
            if ( is_bool($sub_result) )
            {
                //We got a result replace that part in the Query
                $this->q = str_replace($ql, (int)$sub_result, $this->q );
            }

        }
        //Add space to tokens
        $this->q = preg_replace('~(AND|OR|0|1)~', ' $1 ', $this->q);
        rs_log(6,"Q:".$this->q);
        $this->q = str_ireplace(array('and','or'), array('&&','||'),  $this->q);
        //Remove any remaining junk
        $this->q = preg_replace('[^&| 01\)\(]', '' ,$this->q);
        rs_log(6,$this->q);

        try
        {
            //Using this class to aviod php eval
            $this->evaluator = new Evaluator($this->q,' == $var');
            $result = $this->evaluator->evaluate(array('$var'=>1));
            rs_log(6,"R: ".$result);
            return ($result == 1) ? true : false;
        }
        catch (Exception $e)
        {
            rs_log(4,"Evaluator error:".$e->getMessage(). '  Original:  '.$this->query.'  Reduced: '.$this->q.'  Post ID:'.$this->post->ID);
            return null; // For the tester
        }

    }



    /**
     * Validates Query logic for parentheses and Regexp for syntax
     *
     * @return bool
     */
    public function validate()
    {

        //Validate parentheses
        $sum = 0;
        for (  $i = 0; $i < strlen($this->q); $i++ )
        {
            $char = $this->q[$i];
                if ( $char === '(' )
                {
                    $sum = $sum + 1;
                }
                if ($char === ')')
                {
                    $sum = $sum - 1;
                }
                if ($sum == -1 )
                {
                    return false;
                }
        }
        if ($sum === 0)
        {
            //Carbon coppy from admin JS.
            $token = '(([a-z_]*)\.([a-z_]*))';
            $in_type = '((IN|NOT(\s*)IN)(\s+)(\((\s*)((("[^"]+")|([0-9]+))(\s*),?(\s*)?)+\)))';
            $eq_type = '((=|!=)(\s+)(("([^"]+)")|([0-9]+)))';
            $block = '('. $token . '(\s+)(' . $eq_type . '|' . $in_type . '))';
            $grouper = '(\s*)?((\(|\))*)?(\s*)?';
            $block = $grouper . $block  . $grouper;
            $regexp = '#^(\s*)?'. $block .'(((AND|OR)'. $block. ')*)?(\s*)?$#i';
            return (bool)preg_match($regexp, $this->q);
        }
        else
        {
            return false;
        }

    }



}