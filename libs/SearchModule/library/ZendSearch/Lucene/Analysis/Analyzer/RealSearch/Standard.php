<?php
namespace ZendSearch\Lucene\Analysis\Analyzer\RealSearch;

use ZendSearch\Lucene\Analysis;
use ZendSearch\Lucene\Analysis\Analyzer\RealSearch;
use ZendSearch\Lucene\Analysis\TokenFilter;


class Standard extends AbstractRealSearch
{
    /**
     * Current position in a stream
     *
     * @var integer
     */
    private $_position;
    private $_multiTokens = array();

    /**
     * Reset token stream
     */
    public function reset()
    {
        $this->_position = 0;

        if ($this->_input === null) {
            return;
        }

        // convert input into ascii
        if (PHP_OS != 'AIX') {
            $this->_input = iconv($this->_encoding, 'ASCII//TRANSLIT', $this->_input);
        }
        $this->_encoding = 'ASCII';
    }

    /**
     * Tokenization stream API
     * Get next token
     * Returns null at the end of stream
     */
    public function nextToken()
    {

        if ($this->_input === null) {
            return null;
        }

        //If we have MultiTokens return one of them
        if (isset($GLOBALS['rs_tokens']) && !empty($GLOBALS['rs_tokens']))
        {
            $item = array_shift($GLOBALS['rs_tokens']);
            $item->setPositionIncrement(0);
            return $item;
        }

        do {
            if (! preg_match('/\p{L}+/ui', $this->_input, $match, PREG_OFFSET_CAPTURE, $this->_position)) {
                return null;
            }

            $str = $match[0][0];
            $pos = $match[0][1];
            $endpos = $pos + strlen($str);
            $this->_position = $endpos;

            $raw_token = new Analysis\Token($str, $pos, $endpos);
            $token = $this->normalize($raw_token);
        } while ($token === null); // try again if token is skipped
        return $token;
    }
}


?>