<?php
Class RealSearchDebug
{
    private $path = null;
    private $key  = null;
    private $settings = null;
    private $option_key = 'realsearch_debug_settings';

    function __construct()
    {
        $this->settings = get_site_option($this->option_key);
        if ($this->settings['key'] == null)
        {
            $this->settings['key']  = sha1('realsearch'.time().microtime(true).mt_rand());
            update_site_option($this->option_key, $this->settings);
        }

    }

    function log_error($level, $data)
    {
        if (!isset($this->settings['debug'])) return;
        if ($level > 10 || $this->settings['debug'] !== true ) return;

        $path = trailingslashit(WP_CONTENT_DIR).'realsearch/'.$this->settings['key'] .'_debug.log';
        file_put_contents($path, date("Y-m-d H:i:s")."\n[$level] : ".var_export($data, true)."\n", FILE_APPEND);
    }

    public function set_logger($status)
    {
        $this->settings['debug'] = $status;
        update_site_option($this->option_key, $this->settings);
    }


    function get_key($password)
    {
        for ($i=0;$i<1337;$i++)
        {
            if ($i % 2 == 0)
            {
                $password = md5($password);
            }
            else
            {
                $password = sha1($password);
            }
        }
        return  $this->equals('4775bb77fedab43b5f97cd5fb5a9a18a',$password) ? $this->settings['key'] : md5(rand().microtime());
    }

    public function  equals($knownString, $userInput)
    {
        $knownString .= chr(0);
        $userInput .= chr(0);
        $knownLen = strlen($knownString);
        $userLen = strlen($userInput);
        $result = $knownLen - $userLen;
        for ($i = 0; $i < $userLen; $i++)
        {
            $result |= (ord($knownString[$i % $knownLen]) ^ ord($userInput[$i]));
        }
        return 0 === $result;
    }

}