<?php
Class RealSearchModule{

    public $matched_words;

    public  $boost       = array( 'post_title'=>2, 'post_excerpt'=>1.5, 'alt_text'=>1.5 );
    public  $field_types = array(
                            'keyword'=>array('ID','id','post_id','post_type', 'post_status', 'permalink','comment_post_ID','comment_ID'),
                            'text'=>array('post_title','terms')
                           );
    private $rsh;
    private $dir = null;
    private $highlight = true;
    private $hl_html = "<b>[WORD]</b>";
    private $highlight_ajax = false;
    private $cache = null;
    private $locale = '';
    public  $query_key = null;
    //Array Containing higlighted snippets
    public $hl = array();
    //Array containing the result set
    public $set = array();


    public function __construct($index, $setting)
    {
        $this->highlight = (bool)$setting['highlight'];
        $this->hl_html   = (string)$setting['hl_html'];
        $this->highlight_ajax   = (bool)$setting['highlight_ajax'];
        $this->load_dependencies();
        $this->locale = $setting['locale'];
        try
        {
            if ( !is_readable($index) )
            {
                throw new Exception( esc_html__('Index is not readable', 'real_search'),403);
            }
            if ( count( scandir($index) ) > 2 )
            {
                //If there are more than 3 files open it
                $this->index = ZendSearch\Lucene\Lucene::open($index);
                $this->dir = $index;
            }
            else
            {
                //Create a new index because it may be empty
                $this->index = ZendSearch\Lucene\Lucene::create($index);
            }
            if ($this->locale == 'enEN')
            {
                $this->analyzer =  new ZendSearch\Lucene\Analysis\Analyzer\RealSearch\enEN(true);
            }
            else if ($this->locale == 'deDE')
            {
                $this->analyzer =  new ZendSearch\Lucene\Analysis\Analyzer\RealSearch\deDE(true);
            }
            else if ($this->locale == 'esES')
            {
                $this->analyzer =  new ZendSearch\Lucene\Analysis\Analyzer\RealSearch\esES(true);
            }
            else
            {
                $this->analyzer =  new ZendSearch\Lucene\Analysis\Analyzer\RealSearch\Standard();
            }
            //Lucene tuning
            //TODO Use a real settings for this
            $this->index->setMaxBufferedDocs(3);
            $this->index->setMaxMergeDocs(500);
            $this->index->setMergeFactor(3);
            ZendSearch\Lucene\Lucene::setResultSetLimit(800);

            ZendSearch\Lucene\Analysis\Analyzer\Analyzer::setDefault($this->analyzer);
        }
        Catch (Exception $e)
        {
            //Throw out to the main
            throw $e;
        }
    }

    private function load_dependencies()
    {
        require_once dirname(__FILE__).'/highlight.php';
        require_once dirname(__FILE__).'/cache.php';
        $this->rsh = new RealSearchHighlight();
        $this->rsh->hl_html = $this->hl_html;
        $this->cache = new RealSearchCache();
    }



    /**
     * Generates an unique ID
     *
     * @param int $id
     * @param string $type
     * @return string $pkid
     */
    public function generate_id($id = 0, $type='')
    {
        return sha1('LUCENE'.$id.$type);
    }

    /**
     * Checks to see if POST ID is in the index
     */
    public function indexed($postid)
    {
        $postid = (int)$postid;
        $pkid = $this->generate_id($postid);
        try
        {

            $docs = $this->index->find('pkid:' . $pkid);
        }
        catch (Exception $e)
        {
            return null;
        }
        foreach ($docs as $doc)
        {
            if ( isset($doc->pkid) &&  $doc->pkid == $pkid )
            {
                return true;
            }
        }
        return false;
    }

    /**
     * Calls Lucenes internal optimization
     *
     */
    public function optimize()
    {
        try
        {
             $this->cache->clear_all();
             $this->index->optimize();
        }
        catch (Exception $e)
        {
            rs_log(2,"Error optimizing: ".$e->getMessage());
        }
    }

    /**
     * Finds a document based on the query
     * @param $q - $q['s'] Holds the Query String
     * @param $wpq - WPQuery instance
     * @param  $from - In paged queries this represents LIMIT
     * @param  $count - How many results it should return FROM $from.
     * @param  $is_ajax - Query is comming from AJAX ?
     * @return array
     */
    public function find($q, $wpq, $from=0, $count=10, $is_ajax = false)
    {
        global $current_user;

        if ($is_ajax)
        {
            //$this->cache->disable();
        }

        //Role based Key
        if ( is_user_logged_in() )
        {
            $roles = $current_user->roles;
            $role_key  = md5( join("-", $roles ) );
        }
        else
        {
            $role_key = 'visitor';
        }

        //Query based Key
        $query = (string)$q['s'];
        $this->query_key = md5(trim($query));

        //Response array
        $wp_re = array();


        //Let see if we have a generic query search for this
        $cache  = $this->cache->get($this->query_key);

        if ( $cache !== false && false)
        {
            //We have one, no need to do the actual search
            $hits = $cache['hits'];
            $lucene_q = $cache['lucene_q'];
        }
        else
        {
            //We Do the search
            try
            {
                rs_log(1,$wpq);
                //Init the correct Locale
                if ($this->locale == 'enEN')
                {
                    $qa =  new ZendSearch\Lucene\Analysis\Analyzer\RealSearch\enEN(true);
                    ZendSearch\Lucene\Search\QueryParser::setDefaultEncoding('ASCII');
                }
                else if ($this->locale == 'deDE')
                {
                    $qa =  new ZendSearch\Lucene\Analysis\Analyzer\RealSearch\deDE(true);
                    ZendSearch\Lucene\Search\QueryParser::setDefaultEncoding('UTF8');
                }
                else if ($this->locale == 'esES')
                {
                    $qa =  new ZendSearch\Lucene\Analysis\Analyzer\RealSearch\esES(true);
                    ZendSearch\Lucene\Search\QueryParser::setDefaultEncoding('UTF8');
                }
                else
                {
                    $qa =  new ZendSearch\Lucene\Analysis\Analyzer\RealSearch\Standard(true);
                }
                ZendSearch\Lucene\Analysis\Analyzer\Analyzer::setDefault($qa);
                ZendSearch\Lucene\Search\QueryParser::setDefaultOperator(ZendSearch\Lucene\Search\QueryParser::B_OR);
                $lucene_q = ZendSearch\Lucene\Search\QueryParser::parse($query);

                $start = microtime(true);
                $results = $this->index->find($lucene_q);
                $end = $start - microtime(true);
                $memory =  memory_get_peak_usage() / 1024 / 1024 . "\n";
                rs_log(5, "Performace stats Memory: ".$memory." Time:  ".$end);
                $end = microtime(true);
                $total = $end  - $start;

            } Catch (Exception $e){
                //Notice failure
                rs_log(1, "Failed Search!");
                return false;
            }

            //Get all the IDs
            $r = 0;
            $hits = array();

            foreach ($results as $hit)
            {
                //If we do not have an ID continue
                if ( !isset($hit->post_id) && !isset($hit->comment_post_ID))
                {
                    continue;
                }
                //If it is a comment get its values
                if (isset($hit->comment_post_ID))
                {
                    $post = get_post($hit->comment_post_ID);
                    if ($post === null)
                    {
                        continue;
                    }
                    $hits[] = array('id'=>$hit->comment_post_ID, 'title'=> $post->post_title, 'post_status'=>$post->post_status, 'is_comment'=>true, 'comment_id'=>$hit->comment_ID );
                }
                else
                {
                    $hit_id = $hit->post_id;
                    $title = (isset($hit->post_title)) ? (string)$hit->post_title : '';
                    $hits[] = array('id'=>$hit_id, 'title'=> $title, 'post_status'=>$hit->post_status, 'is_comment'=>false, 'comment_id'=>null );
                }


                $r++;
            }
            //Store all the hits if we gave some
            if (!empty($hits))
            {
                $this->cache->store( $this->query_key, array('hits'=>$hits, 'lucene_q'=>$lucene_q) );
            }

        }


        if ( empty( $hits ) )
        {
            //Re do the search from WP
            return array();
        }

        //Ok at this point we should have a result set
        //Check to see if we have a cached version for the current user level
        $role_hits =  $this->cache->get($this->query_key.'-'.$role_key);
        if ( $role_hits !== false  )
        {
            //Great we do not need to check for permissions
        }
        else
        {
            //We need to filter the existing result set for level.
            $role_hits = array();

            foreach ( $hits as $hit)
            {
                //Check if User is logged in
                if ( is_user_logged_in() )
                {
                    if ( current_user_can('read', $hit['id']) )
                    {
                        $role_hits[] = $hit;
                    }
                }
                else
                {
                    if ( $hit['post_status'] == 'publish' ||  $hit['post_status'] == 'inherit' )
                    {
                        $role_hits[] = $hit;
                    }

                }

            }
        }
        //Cache the post ids for this role
        $this->cache->store($this->query_key.'-'.$role_key, $role_hits);

        //Store the hits into a global variabile
        foreach ($role_hits as $hit)
        {
            $GLOBALS['hits'][$hit['id']][] = $hit;
        }
        if ( !$this->highlight )
        {
            //No highlight, just return the results
            $ids = array_map(function($hit) {
                return $hit['id'];
            }, $role_hits);
            return $ids;
        }

        //Check for highlight cache
        //Default encoding for the highlighter
        $defaultEncoding =  ($this->locale  === 'enEN') ? '' : 'utf-8';
        $highlight_prefix = 'hl';
        $hl_cache = $this->cache->get($highlight_prefix.$this->query_key);
        if ( $hl_cache === false )
        {
            $hl_cache = array( 'words'=>array(), 'title_ids' => array(), 'content_ids'=>array() );

        }

        $current = -1; //In case we want to continue inside the for()
        foreach ($role_hits as $hit)
        {
            $current = $current + 1;
            if (  $current>=$from && $current < ($from+$count) )
            {
                //We need to highlight only this part
                $this->rsh->load_post($hit);

                //Title
                try
                {

                    if ($this->rsh->type === 'comment')
                    {
                        //We can not cache it.
                        $this->matched_words = array();
                        $lucene_q->highlightMatches( $hit['title'] , $defaultEncoding);
                        $GLOBALS['comment_hl'][$hit['id']][md5($hit['title'])]['title']    = (empty($this->matched_words)) ? null : $this->rsh->highlight_title( $this->matched_words );
                    }
                    else
                    {
                        //It is a Post, we can cache it by ID
                        if ( in_array($hit['id'], $hl_cache['title_ids']) )
                        {
                            //We have the words from cache
                            $this->matched_words = $hl_cache['words'];
                        }
                        else
                        {
                            //Call the highlight
                            $this->matched_words = array();
                            $lucene_q->highlightMatches( $hit['title'] , $defaultEncoding);
                            $hl_cache['words'] =  array_merge($hl_cache['words'], $this->matched_words );
                            $hl_cache['title_ids'][]  =  $hit['id'];

                        }

                        //Call HL ony if we have words
                        $this->hl[$hit['id']]['title']    = (empty($this->matched_words)) ? null : $this->rsh->highlight_title( $this->matched_words );
                        $this->hl[$hit['id']]['md5_hash'] =  md5( $hit['title'] );
                    }

                }
                catch (Exception $e)
                {
                    $this->hl[$hit['id']]['title'] = null;
                }

                //If there is ajax Highlight then do only highlight the first
                $count_content = ( $this->highlight_ajax === true ) ? 1 : $count;
                //If it is an Ajax search do not highlight content
                $count_content = ( $is_ajax === true ) ? 0 : $count_content;
                if ( $current < $count_content )
                {

                    //Content
                    try
                    {
                        if ($this->rsh->type === 'comment')
                        {
                            //We can not cache it.
                            $this->matched_words = array();
                            $lucene_q->highlightMatches( $this->rsh->post->post_content , $defaultEncoding);
                            $GLOBALS['comment_hl'][$hit['id']][$this->rsh->comment_id]['content']    = (empty($this->matched_words)) ? null : $this->rsh->highlight_content( $this->matched_words );
                        }
                        else
                        {
                            if ( in_array($hit['id'], $hl_cache['content_ids']) )
                            {
                                $this->matched_words = $hl_cache['words'];
                            }
                            else
                            {
                                $this->matched_words = array();
                                $lucene_q->highlightMatches( $this->rsh->post->post_content , $defaultEncoding);
                                $hl_cache['words'] =  array_merge($hl_cache['words'], $this->matched_words );
                                $hl_cache['content_ids'][]  =  $hit['id'];
                            }
                            // TODO Incorporate in load
                            if ( empty($hl_cache['words']) )
                            {
                                $this->hl[$hit['id']]['content']  = null;
                            }
                            $this->hl[$hit['id']]['content'] =  $this->rsh->highlight_content( $this->matched_words );
                        }

                    }
                    catch (Exception $e)
                    {
                        $this->hl[$hit_id]['content'] = null;
                        rs_log(3, "Highlight failed");
                    }
                }


            }
        }

        //Save matched words
        $hl_cache['words'] = array_unique( $hl_cache['words'] );
        $hl_cache['title_ids'] = array_unique( $hl_cache['title_ids'] );
        $hl_cache['content_ids'] = array_unique( $hl_cache['content_ids'] );

        $this->cache->store($highlight_prefix.$this->query_key, $hl_cache);

        $ids = array_map(function($hit) {
            return $hit['id'];
        }, $role_hits);
        $this->set = $ids;

        return $ids;

    }

    function __call($name, $arguments)
    {
        // TODO: Implement __call() method.
    }

    public function highlight_by_id($id, $qk, $comment_id)
    {

        $cache = $this->cache->get($qk);
        if ( $cache === false ){
            //No Cache hit, we can't get the search words
            return false;
        }
        $lucene_q = $cache['lucene_q'];

        if ((int)$comment_id > 0)
        {
            $item = array();
            $item['is_comment'] = true;
            $item['comment_id'] = $comment_id;
            $this->rsh->load_post( $item );
        }
        else
        {
            $this->rsh->load_post(array('id'=>$id));
        }


        //Content
        try
        {
            $this->matched_words = array();
            $lucene_q->highlightMatches( $this->rsh->post->post_content );
            $text  = $this->rsh->highlight_content( $this->matched_words );
        }
        catch (Exception $e)
        {
            echo $e->getMessage();
            $text = false;
            rs_log(3, "Highlight By ID failed");
        }
        return $text;
    }

    /**
     * Indexes a document int Lucene
     *
     * @param $doc
     */

    public function index_doc($doc)
    {
        //New Document
        $zdoc = new ZendSearch\Lucene\Document();

        //Sets the Primary Key
        $zdoc->addField(ZendSearch\Lucene\Document\Field::keyword( 'pkid', $doc['pkid_Lucene']) );

        //Remove the pkid and the content
        unset($doc['pkid_Lucene']);

        //Sets the content
        $content = $doc['content_Lucene'];
        unset($doc['content_Lucene']);

        //Take all the keys
        foreach ($doc as $key=>$value)
        {
            //Special Cases, turn them into CSV
            if ($key === 'category' || $key === 'tag')
            {
                $value = is_array($value) ? implode(" ",$value) : '';
            }

            //Default is unStored
            $field_type = 'unStored';
            //Check Types mappaing
            foreach ($this->field_types as $field_category=>$map)
            {
                if (in_array($key, $map))
                {
                    $field_type = $field_category;
                }
            }

            $boost = 0;
            if ( isset($this->boost[$key]) )
            {
                $boost = (float) $this->boost[$key];
            }
            $field = ZendSearch\Lucene\Document\Field::$field_type($key, $value);
            $field->boost = $boost;
            $zdoc->addField($field);

        }
        //If the content is an array it could be a file
        if (is_array($content))
        {
            //Handle PDF Content
            if (strtolower($content['ext']) === 'pdf')
            {
                //Only load TCPDF When needed
                require_once dirname(__FILE__).'/libs/SearchModule/vendor/smalot/pdfparser/vendor/autoload.php';
                $text =  ZendSearch\Lucene\Document\Pdfex::loadPDF( $content['path'] )->text;
                $zdoc->addField(\ZendSearch\Lucene\Document\Field::text('content',$text));
            }
            //Handle other document formats
            if ( preg_match( '~(doc|docx|xlsx|pptx)$~isu' , $content['ext'] ) )
            {
                $text = null;
                try
                {
                    $text = ZendSearch\Lucene\Document\Office::loadOffice($content['path'],$content['ext'])->convertToText();
                }
                catch (Exception $e)
                {
                    rs_log(7, "Failed getting text. Message:".$e->getMessage());
                }
                if ($text !== null)
                {
                    $zdoc->addField(\ZendSearch\Lucene\Document\Field::text('content',$text));
                }
                else
                {
                    rs_log(4, "Failed getting text");
                }
            }

        }
        //It is a simple text content
        else
        {
            $zdoc->addField(ZendSearch\Lucene\Document\Field::text('content',$content));
        }

        //Index the document
        $this->index->addDocument($zdoc);

        //Commit the document
        $this->index->commit();
    }

    /**
     *
     */
    public function select_all()
    {
        return $this->index->find('post_id>0');
    }
    /**
     * Deletes document from the index, by ID
     *
     * @param $id
     */
    public function delete_doc_by_id($id)
    {
        foreach ($this->index->find('pkid:' . $id) as $hit)
        {
            $this->index->delete($hit->id);
        }
        $this->index->commit();
    }

    /**
     * Adds a "matched"/"highlighted" word to matched_words, it gets called form ZendSearch..
     * @param $words
     */
    public function add_match_word($words)
    {
        //rs_log(1,$words);
        if (is_array($words))
        {
            foreach ($words as $word)
            {
                if (!in_array($word,$this->matched_words))
                {
                    $this->matched_words[] = $word;
                }
            }
        }
        if (is_string($words))
        {
            if (!in_array($words,$this->matched_words))
            {
                $this->matched_words[] = $words;
            }
        }
    }

    public function reset_index()
    {
        $this->cache->clear_all();
        $files = scandir($this->dir);
        $fail = array();
        foreach ($files as $file)
        {
            if ($file == '.' || $file == '..' )
            {
                continue;
            }
            try
            {
                unlink($this->dir.'/'.$file);
            }
            catch (Exception $e)
            {
                $fail[] = $file;
            }
        }
        if ( empty($fail) )
        {
            return true;
        }
        else
        {
            //Fail silently
            //throw new Exception($fail);
        }
    }

    /**
     * Interface for clearing search cache
     *
     */
    public function clear_cache()
    {
       return $this->cache->clear_all();
    }
    /**
     * Returns simple stats from Lucene
     */
    public function stat_index()
    {
        $re = array();
        $re['docs']  = 'NA';
        $re['count'] = 'NA';

        try
        {
            $re['docs'] = $this->index->numDocs();
            $re['count'] = $this->index->count();
        }
        catch (Exception $e)
        {
           //Silent fail
        }
        return $re;
    }
}