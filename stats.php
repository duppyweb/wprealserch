<?php
Class RealSeachStats
{
    private  $table = 'rs_stats';
    private $option_key = 'real_search_stats';
    //Query get Parameter
    private $s = 's';
    private $site_id = 0;

    public function __construct()
    {
        global $wpdb;
        $this->site_id = get_current_blog_id();
        $this->table = $wpdb->prefix.$this->table;
    }

    public function log_search($query, $count)
    {
        global $wpdb;
        $query = $this->normalize( (string)$query[$this->s]);
        $data = array(
            'site_id' => $this->site_id,
            'query'   => $query,
            'hash'    => md5($query.time()),
            'result'  => $count,
            'hits'    => 0
        );
        $wpdb->insert($this->table, $data);
        return $wpdb->insert_id;
    }

    public function track($id)
    {
        global $wpdb;
        $id = (int)$id;
        $sql = $wpdb->prepare("UPDATE {$this->table} SET hits = hits + 1 WHERE
                               id = %d AND
                               stamp > DATE_SUB(CURDATE(), INTERVAL 1 HOUR )",
                              array($id));
        $wpdb->query( $sql );
    }

    /**
     * @param $slot
     * @param $period
     * @return array|null|object
     */
    public function histogram($slot, $period)
    {
        global $wpdb;
        $slot = (int)$slot;
        $period = (int)$period;
        $period = time() - $period;

        //die("$slot =  ".date("Y-m-d H:i:s ",$period));
        $sql = $wpdb->prepare("SELECT (UNIX_TIMESTAMP(stamp) DIV %d) AS slot,
                               COUNT(*)  AS searches,
                               SUM(hits) as clicks
                               FROM   {$this->table}
                               WHERE site_id = %d AND stamp > %s
                               GROUP  BY slot", array( $slot, $this->site_id, date("Y-m-d H:i:s",$period) ) );
        rs_log(5,$sql,"Stats SQL");
        $result = $wpdb->get_results($sql, ARRAY_A);
        rs_log(5,$result,"Stats result");
        return $result;
    }

    /**
     * Turns a Query into Array and normalizes the terms
     */
    private function query_to_array($query)
    {
        $query = mb_strtolower($query);
        $query = preg_replace( "~([^a-zA-Z0-9 ]+)~", '', $query );
        $query = explode(" ", $query);
        $query = array_map('trim',$query);
        return $query;
    }

    public function tag_cloud()
    {
        global $wpdb;
        $last_stamp = 0;
        $cloud = array();
        $sql = $wpdb->prepare("SELECT query FROM {$this->table} WHERE stamp > %d", array($last_stamp));
        $rows = $wpdb->get_results($sql, ARRAY_A);

        foreach ($rows as $row)
        {
            $aq = $this->query_to_array($row['query']);
            foreach ($aq as $word)
            {
                if ( isset($cloud[$word]) )
                {
                    $cloud[$word]= $cloud[$word] +  1;
                }
                else
                {
                    $cloud[$word] = 1;
                }
            }
        }
        $total = array_sum($cloud);
        uasort($cloud, array($this,'compare'));
        $cloud = array_slice($cloud,0,100);
        return $cloud;
    }
    private function compare($a,$b)
    {
        if ($a == $b) {
            return 0;
        }
        return ($a > $b) ? -1 : 1;
    }

    private function normalize($str)
    {
        return trim($str);
    }

    public function get_table_name()
    {
        return $this->table;
    }
}