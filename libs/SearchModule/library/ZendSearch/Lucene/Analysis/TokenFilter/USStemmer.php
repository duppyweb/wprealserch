<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * @package   Zend_Search
 */

namespace ZendSearch\Lucene\Analysis\TokenFilter;
use phpMorphy;
use phpMorphy_FilesBundle;
use ZendSearch\Lucene;
use ZendSearch\Lucene\Analysis\Token;
use ZendSearch\Lucene\Exception\InvalidArgumentException;
use ZendSearch\Lucene\Exception\RuntimeException;

/**
 * Lower case Token filter.
 *
 * @category   Zend
 * @package    Zend_Search_Lucene
 * @subpackage Analysis
 */
class USStemmer implements TokenFilterInterface
{

    private $morphy;
    private $mutiTerm;
    public function __construct($mutiTerm = false)
    {
        $this->mutiTerm = $mutiTerm;
        $this->morphy = new phpMorphy(
            new phpMorphy_FilesBundle(realpath(PHPMORPHY_DIR . '/../../en/'), 'eng'),
            array('storage' => PHPMORPHY_STORAGE_FILE));


    }
    public function normalize(Token $srcToken)
    {


        $text = $srcToken->getTermText();
        $text =  mb_strtoupper(trim($text), 'UTF-8');

        $basetext = $this->morphy->getBaseForm($text);

        //We got multiple hits
        if (is_array($basetext))
        {
            //Remove the same
            foreach ($basetext as $key=>$value)
            {
                if ($value === $text)
                {
                    unset($basetext[$key]);
                }
            }
        }

        $multi_hit = false;
        if (count($basetext) > 1 )
        {
            $multi_hit = true;
        } else
        {
            $basetext = $basetext ? mb_strtolower(array_shift($basetext), 'UTF-8') : null;
        }

        //Default value
        if ($basetext == null)
        {
            $basetext = mb_strtolower($text, 'UTF-8');
        }

        $debug = "[Stemmer]: IN: ".strtolower($text)." OUT: $basetext";

        if (defined('REALSEARCH_DEBUG') && REALSEARCH_DEBUG == True )
        {
            rs_log(21,$debug);
        }
        if ($multi_hit && $this->mutiTerm)
        {
            $hits = array();
            //Create an array of tokens
            foreach ($basetext as $tag)
            {
                $tag = mb_strtolower($tag, 'UTF-8');
                $hits[] = new Token( $tag,
                    $srcToken->getStartOffset(),
                    $srcToken->getEndOffset());
            }
            //Store the tags Globally
            $GLOBALS['rs_tokens'] = $hits;
            //Return the first one with 1 as Increment
            $return =  array_shift($GLOBALS['rs_tokens']);
            $return->setPositionIncrement(1);
            return $return;

        }
        else
        {
            $newToken = new Token( $basetext,
                $srcToken->getStartOffset(),
                $srcToken->getEndOffset());

            $newToken->setPositionIncrement(1);
        }



        return $newToken;
    }
}

