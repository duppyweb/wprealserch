<?php
Class RealSearchHighlight {

    public  $extract_text = array('post', 'page', 'attachment');
    public  $post = null;
    public  $hl_html = "<b>[WORD]</b>";
    public  $type = null;
    public  $comment_id  = 0;
    private $db = 0;

    /**
     * Loads a post into $post
     * @param $item
     */
    public function load_post( $item )
    {
        if ($item['is_comment'] == true)
        {
            $this->post = get_comment($item['comment_id']);
            $this->comment_id = $item['comment_id'];
            $this->post->post_content = $this->post->comment_content;
            $parent_post = get_post($this->post->comment_post_ID);
            $this->post->post_title = esc_html__('Comment on, ', 'real_search') .$parent_post->post_title;
            $this->type = 'comment';
        }
        else
        {
            $this->post =  get_post( $item['id'] );
            $this->type = 'post';
        }

    }

    /**
     * Highlights the Title
     * @param $words array
     * @return string Highlighted title to show on result pages
     */
    public function highlight_title($words)
    {
        if ( isset( $this->post->post_title ) )
        {
            $title = apply_filters( 'the_title', $this->post->post_title );
            $title = $this->highlight($this->post->post_title, $words);
        }
        else
        {
            $title = null;
        }
        return $title;
    }

    /**
     * Highlights the Post Content
     * @param $words
     * @return string Highlighted content to show on result pages
     * Note: on empty string the main hook will return the original excerpt.
     */
    public function highlight_content($words)
    {
        //Extract text based on post type
        $post_content = '';
        if ( isset( $this->post->post_content ) )
        {
            if ( in_array( (string)$this->post->post_type, $this->extract_text ) )
            {
                $post_content = apply_filters( 'the_content', $this->post->post_content );
                $post_content = strip_tags($post_content);
                $post_content = $this->highlight($post_content, $words);
                $post_content = html_entity_decode($post_content);
            }
        }
        return  $post_content;

    }

    /**
     * Filters content using WordPress specific functions
     * @param $content
     * @return mixed|string|void
     */
    public function filter_content($content)
    {
        $content = apply_filters('the_content', $content);
        //$content = strip_tags($content);
        return $content;
    }

    /**
     * Highlights matched words in a text
     *
     * @param $text
     * @param $words array
     * @return string highlighted text
     * @credit http://stackoverflow.com/questions/1292121/how-to-generate-the-snippet-like-generated-by-google-with-php-and-mysql
     * @credit http://stackoverflow.com/questions/5032210/php-sentence-boundaries-detection
     */
    private function highlight($text,$words)
    {
        //rs_log(1,array("text"=>$text, 'words'=>$words));
        $this->db = $this->db + 1;


        //Get HL_HTML
        $hl_html  = $this->hl_html;
        $hl_html = str_replace('[WORD]', '$0', $hl_html);

        //header("Content-Type: text/plain;");
        //Filter words smaller than 3 chars
        foreach ((array)$words as $key=>$word)
        {
            if ( mb_strlen($word) < 2 )
            {
                unset( $words[$key]);
            }
        }
        if ( count($words) == 0)
        {
            return null;
            //return "ZERO_WORDS";
        }
        $length = mb_strlen($text);
        $text = preg_replace("~(\r|\n)~iu","___ . ",$text);
        $words_array = $words;
        array_map("preg_quote",$words);
        $words = join('|', $words);

        //We have a small text
        if ($length < 300)
        {
            $result = preg_replace('#'.$words.'#iu', $hl_html, $text);
            $result = str_replace('___ .', '',$result);
            return $result;
        }

        //Get the full sentences
        $re = '/# Split sentences on whitespace between them.
                (?<=                # Begin positive lookbehind.
                  [.!?]             # Either an end of sentence punct,
                | [.!?][\'"]        # or end of sentence punct and quote.
                )                   # End positive lookbehind.
                (?<!                # Begin negative lookbehind.
                  Mr\.              # Skip either "Mr."
                | Mrs\.             # or "Mrs.",
                | LTD\.             # or "Mrs.",
                | Ms\.              # or "Ms.",
                | Jr\.              # or "Jr.",
                | Dr\.              # or "Dr.",
                | Prof\.            # or "Prof.",
                | Sr\.              # or "Sr.",
                | T\.V\.A\.         # or "T.V.A.",
                                    # or... (you get the idea).
                )                   # End negative lookbehind.
                \s+                 # Split on whitespace between sentences.
                /ix';


        $sentences = preg_split($re, $text, -1, PREG_SPLIT_NO_EMPTY);
        //TODO If a sentece is bigger than
        $sentences = array_unique($sentences);
        $results = array();
        $index = 0;
        foreach ($sentences as $sentence)
        {
            $index = $index + 1;
            //Count hits per sentence
            preg_match_all("~(($words)+)~isu", $sentence, $mat);
            if ( isset( $mat[2] ) )
            {
                $count_total = count( $mat[2] );
                $count_uniq  = count( array_unique( $mat[2] ) );
                if ( $count_total > 0)
                {
                    $results[] = array( 'line'=> $sentence, 'count'=>$count_total, 'count_uniq'=>$count_uniq );
                }

            }
        }

        //Weight counted
        $max_score = 0;
        $start = 0;
        foreach ( $results as $key=>$values )
        {
            $score = pow($values['count'], $values['count_uniq']);
            if ( $max_score < $score )
            {
                $max_score = $score;
                $start = $key;
            }
        }


        //Remove dupes
        for ($i = 0; $i < count( $results ); $i++)
        {
            $dupe = null;
            for ($j = $i+1; $j < count( $results ); $j++)
            {
                if (strcmp( $results[$j]['line'], $results[$i]['line'] ) === 0)
                {
                    $dupe = $j;
                    break;
                }
            }
            if ( !is_null( $dupe ) )
            {
                array_splice( $results, $dupe, 1 );
            }

        }


        if ( count($results) > 4)
        {
            $results = array_splice( $results, $start, 4 );
        }



        $join_token = ' ... ';
        $stripped_text = '';
        $prev_index = -1;
        foreach ( $results as $result )
        {
           /* if ($results['index'] == $prev_index + 1 ){
                $stripped_text = $stripped_text . $result['line'];
            } else {
                $stripped_text = $stripped_text . $join_token . $result['line'];
            }
            $prev_index = $result['index'];*/

            $stripped_text = $stripped_text . $join_token . $result['line'];
        }



        $stripped_text = preg_replace('~('.$words.')~isu', $hl_html, $stripped_text);
        rs_log(7,"After MARK:".$stripped_text);
        //Remove new line identfier
        return str_replace('___ .', '',$stripped_text);
    }


}