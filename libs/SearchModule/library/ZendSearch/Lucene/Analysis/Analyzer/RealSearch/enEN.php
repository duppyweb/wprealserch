<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/zf2 for the canonical source repository
 * @copyright Copyright (c) 2005-2012 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 * @package   Zend_Search
 */

namespace ZendSearch\Lucene\Analysis\Analyzer\RealSearch;

use ZendSearch\Lucene\Analysis\Analyzer\RealSearch;
use ZendSearch\Lucene\Analysis\TokenFilter;


/**
 * @category   Zend
 * @package    Zend_Search_Lucene
 * @subpackage Analysis
 */
class enEN extends Standard
{
    public function __construct($multiTerm = false)
    {
        //Lowercase
        $this->addFilter( new TokenFilter\LowerCase() );

        //StopWords
        $stop_words_filter = new TokenFilter\StopWords();
        $stop_words_filter->loadFromFile(dirname(__FILE__).'/StopWords/en_EN.txt');
        $this->addFilter( $stop_words_filter );

        //Stemmer
        $this->addFilter( new TokenFilter\USStemmer($multiTerm) );
    }
}

