<?php
Class RealSerachDashboard
{
    private $settings = array();
    public  $activated = false;
    public function __construct($settings, $version)
    {
        $this->version = $version;
        $this->settings = $settings;

    }

    public function js_messages()
    {
        $messages = array(
            "AJAX_ERROR"        => esc_html(__("Ajax Error, please retry.", 'real_search')),
            "FAIL_GET_SETTINGS" => esc_html(__("Failed getting settings, please retry.", 'real_search')),
            "FAIL_SET_SETTINGS" => esc_html(__("Error getting server settings, please try again.", 'real_search')),
            "REINDEX_HALTED"    => esc_html(__("Re-indexing halted", 'real_search')),
            "REINDEXING"        => esc_html(__("Reindexing...", 'real_search')),
            "OK_REINDEXING"     => esc_html(__("Document reindexing done.", 'real_search')),
            "OK_SET_SETTINGS"   => esc_html(__("Settings saved.", 'real_search')),
            "OK_GET_SETTINGS"   => esc_html(__("Got settings.", 'real_search')),
            "FAIL_OPTIMIZE"     => esc_html(__("Index optimization failed.", 'real_search')),
            "OK_OPTIMIZE"       => esc_html(__("Index optimized.", 'real_search')),
            "OPTIMIZING"        => esc_html(__("Optimizing...", 'real_search')),
            "OK_RESET"          => esc_html(__("Index deleted", 'real_search')),
            "FAIL_RESET"        => esc_html(__("Failed deleting the index, maybe it was in use?", 'real_search')),
            "FAIL_GET_SELECTOR" => esc_html(__("Failed getting selectors", 'real_search')),
            "ACTIVATING"        => esc_html(__("Activating", 'real_search')),
            "ACTIVATING_OK"     => esc_html(__("Plugin activated! Thank you for your purchase.", 'real_search')),
            "ACTIVATING_FAIL"   => esc_html(__("Plugin activation failed, if you need assistance contact support at support@wprealsearch.com", 'real_search')),
            "CLOUD_FAIL"        => esc_html(__("Failed getting the tag cloud.", 'real_search')),
            "CLEARING"          => esc_html(__("Clearing search cache from transients.", 'real_search')),
            "OK_CLEAR_CACHE"    => esc_html(__("Search cache cleared.", 'real_search')),
            "FAIL_CLEAR_CACHE"  => esc_html(__("Error clearing search cache.", 'real_search')),
            "CONFIRM_DROP"      => esc_html(__("Are you sure you want to drop the index?", 'real_search')),
            "REINDEX_LOCKED"    => esc_html(__("Settings changed, please Save settings before Reindex", 'real_search')),


            "DT_emptyTable"     => esc_html(__("No data available in table", 'real_search')),
            "DT_info"           => esc_html(__("Showing _START_ to _END_ of _TOTAL_ entries ", 'real_search')),
            "DT_infoEmpty"      => esc_html(__("Showing 0 to 0 of 0 entries", 'real_search')),
            "DT_infoFiltered"   => esc_html(__("(filtered from _MAX_ total entries)", 'real_search')),
            "DT_thousands"      => esc_html(__(",", 'real_search')),
            "DT_lengthMenu"     => esc_html(__("Show _MENU_ entries", 'real_search')),
            "DT_loadingRecords" => esc_html(__("Loading...", 'real_search')),
            "DT_processing"     => esc_html(__("Processing...", 'real_search')),
            "DT_search"         => esc_html(__("Search:", 'real_search')),
            "DT_zeroRecords"    => esc_html(__("No matching records found", 'real_search')),

            "DT_paginate_first"      => esc_html(__("First", 'real_search')),
            "DT_paginate_last"       => esc_html(__("Last", 'real_search')),
            "DT_paginate_next"       => esc_html(__("Next", 'real_search')),
            "DT_paginate_previous"   => esc_html(__("Previous", 'real_search')),
            "DT_aria_sortAscending"  => esc_html(__(": activate to sort column ascending", 'real_search')),
            "DT_aria_sortDescending" => esc_html(__(": activate to sort column descending", 'real_search')),

        );
        return $messages;
    }

    public function settings()
    {
        include_once dirname(__FILE__).'/tpl/index.php';
    }

    public function stats()
    {
        include_once dirname(__FILE__).'/tpl/stats.php';
    }

}