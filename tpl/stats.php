<div class="wrap">
    <h1><?php esc_html_e('RealSearch stats', 'real_search') ?> <span><?php esc_html_e('version', 'real_search') ?>: <?php echo $this->version; ?></span></h1>

    <?php esc_html_e('Period', 'real_search') ?>
    <select name="period" id="period">
        <option value="86400"><?php esc_html_e('Today', 'real_search') ?></option>
        <option selected value="604800"><?php esc_html_e('7 Days', 'real_search') ?></option>
        <option value="2592000"><?php esc_html_e('1 Month', 'real_search') ?></option>
        <option value="15552000"><?php esc_html_e('6 Months', 'real_search') ?></option>
        <option value="31536000"><?php esc_html_e('1 Year', 'real_search') ?></option>
    </select>

    <?php esc_html_e('Scale', 'real_search') ?>
    <select name="scale" id="scale">
        <option value="300"><?php esc_html_e('5 Minutes', 'real_search') ?></option>
        <option value="1800"><?php esc_html_e('30 Minutes', 'real_search') ?></option>
        <option selected value="3600"><?php esc_html_e('1 Hour', 'real_search') ?></option>
        <option value="43200"><?php esc_html_e('12 Hours', 'real_search') ?></option>
        <option value="86400"><?php esc_html_e('1 Day', 'real_search') ?></option>
        <option value="604800"><?php esc_html_e('1 Week', 'real_search') ?></option>
    </select>


    <div class="container" style="width:100%">
        <div id="viz_container" class="theme_default z-depth-3"></div>
    </div>

    <hr/>
    <div class="half">
        <h2><?php esc_html_e('WordCloud', 'real_search') ?></h2>
        <div id='wordcloud'></div>
    </div>
    <div class="half">

        <h2><?php esc_html_e('Searches', 'real_search') ?></h2>
        <table id="search_queries">
            <thead>
            <th><?php esc_html_e('Query', 'real_search') ?></th>
            <th><?php esc_html_e('Results', 'real_search') ?></th>
            <th><?php esc_html_e('Clicks', 'real_search') ?></th>
            <th><?php esc_html_e('Date', 'real_search') ?></th>
            </thead>
        </table>
    </div>
</div>
<input type="hidden" name="real_serach_token" id="real_serach_token" value="<?php echo  wp_create_nonce( 'real_search_admin' ); ?>">



