<?php
defined( 'ABSPATH' ) or die( 'Silence is golden!' );
/**
 * Class RealSearchCache
 * Simple Caching interface, based mainly on wp_transient
 */
Class RealSearchCache
{
    private $prefix = 'realsearch_cache_';
    private $ttl = 18000;
    private $enabled = true;

    function store( $key, $value)
    {
        if (!$this->enabled)
        {
            return false;
        }
        $key = $this->prefix.$key;
        try
        {
            set_site_transient( $key, $value, 12 * HOUR_IN_SECONDS  );
        }
        catch (Exception $e)
        {
            throw new Exception("RealSearchCahce Store error: ".$e->getMessage());
        }
    }

    function get( $key )
    {
        if (!$this->enabled)
        {
            return false;
        }
        $key = $this->prefix.$key;
        try
        {
            return  get_site_transient($key);
        }
        catch (Exception $e)
        {
            throw new Exception("RealSearchCahce Store error: ".$e->getMessage());
        }
    }

    function clear_all()
    {
        global $wpdb;
        return $wpdb->query( $wpdb->prepare("DELETE FROM {$wpdb->options} WHERE option_name LIKE %s ", array('%'.$this->prefix."%")) );
    }

    function disable()
    {
        $this->enabled = false;
    }


}